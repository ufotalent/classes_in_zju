#include <Windows.h>
#include "update.h"
#include "utils.h"
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
HWND hWnd;
#define szAppName TEXT("animation")
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,PSTR szCmdLine, int iCmdShow) {    
    
    MSG    msg ;
    WNDCLASS wndclass ;
	wndclass.style        = CS_HREDRAW | CS_VREDRAW ;
	wndclass.lpfnWndProc  = WndProc ;    
	wndclass.cbClsExtra   = 0 ;   
    wndclass.cbWndExtra   = 0 ;
    wndclass.hInstance    = NULL ;
    wndclass.hIcon        = LoadIcon (NULL, IDI_APPLICATION) ;        
	wndclass.hCursor      = LoadCursor (NULL, IDC_ARROW) ;    
	wndclass.hbrBackground= (HBRUSH) GetStockObject (WHITE_BRUSH) ;    
	wndclass.lpszMenuName = NULL ;
    wndclass.lpszClassName= szAppName ;
    if (!RegisterClass (&wndclass)) {
		MessageBox(NULL,TEXT("This program requires Windows NT!"),szAppName, MB_ICONERROR) ;
        return 0; 
    }
    hWnd = CreateWindow( szAppName,      // window class name
                   TEXT("animation"),   // window caption
                   WS_SYSMENU,  // window style
                   CW_USEDEFAULT,// initial x position
                   CW_USEDEFAULT,// initial y position
                   500,// initial x size
                   530,// initial y size
                   NULL,                 // parent window handle
				   NULL,            // window menu handle
				   NULL,   // program instance handle
				   NULL) ;      // creation parameters                   
    ShowWindow (hWnd, SW_SHOWNORMAL) ;
    UpdateWindow (hWnd) ;
    while (GetMessage (&msg, NULL, 0, 0)) {
		TranslateMessage (&msg);
		DispatchMessage (&msg) ;
    }
    return 0;
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_CREATE:
		SetTimer(hwnd,0,30,NULL);
		hWnd=hwnd;
		init();
		break;
	case WM_TIMER:
		update();
		break;
	case WM_CLOSE: 
		exit(0);
	}
	return DefWindowProc (hwnd, message, wParam, lParam) ; 
}

