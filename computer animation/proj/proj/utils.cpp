#include <windows.h>
#include <cmath>
#include <fstream>
#include "utils.h"
using namespace std;
#define PI 3.1415926535897
extern HWND hWnd;
int bombszx=220;
int bombszy=170;
int explszx=1120;
int explszy=1120;
int cloudszx[100]={112,128,104};
int cloudszy[100]={79,84,64};
HDC hdcWindow;
HDC hdcBuffer;
HBITMAP hBitmapBuffer;
#pragma   comment(lib,"Msimg32.lib")
void init() {
	hdcWindow=GetDC(hWnd);
	int *bits;
	bits=(int*) malloc(sizeof(int)*600*600);
	hBitmapBuffer=CreateBitmap(500,500,1,32,bits);
	hdcBuffer=CreateCompatibleDC(hdcWindow);
	SelectObject(hdcBuffer,hBitmapBuffer);
}
void drawBomb(int x,int y) {
	static HBITMAP hbitmap=(HBITMAP)LoadImage(NULL,TEXT("resources/bomb.bmp"),IMAGE_BITMAP,bombszx,bombszy,LR_LOADFROMFILE);
	static HDC hdcbuf=CreateCompatibleDC(hdcWindow);
	SelectObject(hdcbuf,hbitmap);
	BitBlt(hdcBuffer,x-bombszx/2,y-bombszy/2,bombszx,bombszy,hdcbuf,0,0,SRCCOPY);
}

void clear() {
	static bool firsttime=true;
	static HDC hdcWindow,hdc;
	static int *bits;
	static HBITMAP hbitmap;
	if (firsttime) {
		hdcWindow=GetDC(hWnd);
		bits=(int *)malloc(500*500*sizeof(int));
		memset(bits,0xff,500*500*sizeof(int));
		hbitmap=CreateBitmap(500,500,1,32,bits);
		hdc=CreateCompatibleDC(hdcWindow);
		SelectObject(hdc,hbitmap);
		firsttime=false;
	}
	BitBlt(hdcBuffer,0,0,500,500,hdc,0,0,SRCCOPY);
}
void refresh() {
	BitBlt(hdcWindow,0,0,500,500,hdcBuffer,0,0,SRCCOPY);
}
double vectortoarg(int x,int y) {
	if (x==0) {
		if (y==0) return 0;
		if (y>0) return PI/2;
		else
			return PI*3/2;
	}
	if (y>0)
		return acos(x/sqrt(double(x*x+y*y)));
	else
		return 2*PI-acos(x/sqrt(double(x*x+y*y)));

}
void drawFlash(int n,double arc[],double band[],int cx[],int cy[]) {
	static bool firsttime=true;
	static HDC hdcWindow,hdc;
	static int *bits;
	static HBITMAP hbitmap;
	if (firsttime) {
		hdcWindow=GetDC(hWnd);
		hdc=CreateCompatibleDC(hdcWindow);
		bits=(int *)malloc(500*500*sizeof(int));
		firsttime=false;
	}
	memset(bits,0xff,500*500*sizeof(int));
	for (int i=0;i<500;i++) {
		for (int j=0;j<500;j++) {
			for (int s=0;s<n;s++) {
				int dx=i-cx[s];
				int dy=j-cy[s];
				if (dx*dx+dy*dy<35*35)
					continue;
				double arg=vectortoarg(dx,dy);
				if (abs(arg-arc[s])<band[s])
					*(bits+500*j+i)=RGB(0,255,255);
			}
		}
	}
	hbitmap=CreateBitmap(500,500,1,32,bits);
	DeleteObject(SelectObject(hdc,hbitmap));
	TransparentBlt(hdcBuffer,0,0,500,500,hdc,0,0,500,500,0xffffff);
	//BitBlt(hdcBuffer,0,0,500,500,hdc,0,0,SRCCOPY);
}
void drawExplosion(int size) {
	static HBITMAP hbitmap=(HBITMAP)LoadImage(NULL,TEXT("resources/explode.bmp"),IMAGE_BITMAP,explszx,explszy,LR_LOADFROMFILE);
	static HDC hdcbuf=CreateCompatibleDC(hdcWindow);
	SelectObject(hdcbuf,hbitmap);
	TransparentBlt(hdcBuffer,250-size/2,250-size/2,size,size,hdcbuf,0,0,explszx,explszy,0xffffff);
	//BitBlt(hdcBuffer,x-bombszx/2,y-bombszy/2,bombszx,bombszy,hdcbuf,0,0,SRCCOPY);
}
void drawClouds(int n,double x[],double y[], double rate[],int type[],double ratio) {
	static bool firsttime=true;
	static HBITMAP hbitmapcloud[10];
	static int *screenbits=(int *) malloc(600*600*sizeof(int));
	static HDC hdc=CreateCompatibleDC(hdcWindow);
	if (firsttime) {
		firsttime=false;
		for (int i=0;i<3;i++) {
			char filename[100]="resources/cloud";
			int len=strlen(filename);
			filename[len]=i+'0';
			filename[len+1]=0;
			strcat(filename,".bmp");
			hbitmapcloud[i]=(HBITMAP)LoadImage(NULL,filename,IMAGE_BITMAP,cloudszx[i],cloudszy[i],LR_LOADFROMFILE);

		}
	}

	for (int i=0;i<n;i++) {
		SelectObject(hdc,hbitmapcloud[type[i]]);
		int szx=cloudszx[type[i]]*rate[i];
		int szy=cloudszy[type[i]]*rate[i];
		TransparentBlt(hdcBuffer,x[i]-szx/2,y[i]-szy/2,szx,szy,hdc,0,0,cloudszx[type[i]],cloudszy[type[i]],0xffffff);
	}
	if (ratio<0.99) {

		

		HBITMAP hbitmap=(HBITMAP)GetCurrentObject(hdcBuffer,OBJ_BITMAP);
		GetBitmapBits(hbitmap,500*500*4,screenbits);
		for (int x=0;x<500;x++) 
			for (int y=0;y<500;y++) {
				unsigned int c=*(screenbits+x*500+y);
				unsigned int R=c&0xff;
				unsigned int G=(c&0xff00)>>8;
				unsigned int B=(c&0xff0000)>>16;
				if (!(R==255 && G==255 && B==255)) {
//					fout <<">"<< R << " " <<G<<" "<<B<<"\n";

				R=R*ratio+255*(1-ratio);
				G=G*ratio+255*(1-ratio);
				B=B*ratio+255*(1-ratio);
//					fout << R << " " <<G<<" "<<B<<"\n";
				}
				*(screenbits+x*500+y)=R+G*256+B*65536;
			}
		
		HBITMAP b=CreateBitmap(500,500,1,32,screenbits);
		static HDC hdc=CreateCompatibleDC(hdcBuffer);
		SelectObject(hdc,b);
		
		BitBlt(hdcBuffer,0,0,500,500,hdc,0,0,SRCCOPY);
	}
}