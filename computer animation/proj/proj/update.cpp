#include "update.h"
#include "utils.h"
#define PI 3.1415926535897


struct STATE {
	const static int BEGIN=0;
	const static int FLASH=1;
	const static int EXPLODE=2;
	const static int RESTORE=3;
	const static int SPREAD=4;
	const static int END=5;
};
int LENGTH[6]={30,60,20,60,60,30};


void update() {
	static int state=STATE::BEGIN;
	static int tick=0;
	static double flasharc[10];
	static double flashband[10];
	static double flashspeed[10];
	static int flashcx[10];
	static int flashcy[10];
	static int flashnum;
	static double cloudx[20];
	static double cloudy[20];
	static double cloudr[20];
	static double cloudvx[20];
	static double cloudvy[20];
	static int cloudtype[20];

	static double ratio=0;


	tick++;
	if (tick>LENGTH[state]) {
		state++;
		tick=0;
	}
	if (state>STATE::END)
		state=STATE::BEGIN;
	clear();


	switch (state) {
	case STATE::BEGIN:
		drawBomb(250+rand()*5.0/RAND_MAX,250+rand()*5.0/RAND_MAX);
		break;
	case STATE::FLASH: {
		int bx=250+rand()*5.0/RAND_MAX;
		int by=250+rand()*5.0/RAND_MAX;
		drawBomb(bx,by);
		if (tick%20==0) {
			flashnum=tick/20+1;
			flasharc[flashnum-1]=tick*1.0/10/3*PI;
			flashband[flashnum-1]=PI/30;
			flashspeed[flashnum-1]=rand()*1.0/RAND_MAX/100;
			//			flashcx[flashnum-1]=rand()*40.0/RAND_MAX+190;
			//			flashcy[flashnum-1]=rand()*40.0/RAND_MAX+250;
		}

		for (int i=0;i<flashnum;i++) {
			flasharc[i]+=flashspeed[i];
			flashband[i]+=PI/600;
			flashcx[i]=bx;
			flashcy[i]=by;
		}
		drawFlash(flashnum,flasharc,flashband,flashcx,flashcy);
		//double 

		break;
					   }
	case STATE::EXPLODE: 
		drawExplosion(tick*tick*10);
		
		break;
	case STATE::RESTORE:
		if (tick==0) {
			ratio=0;
			for (int i=0;i<20;i++) {
				cloudtype[i]=rand()%3;
				cloudr[i]=(0.6+rand()*0.4/RAND_MAX)*2;
				
				cloudvx[i]=rand()*4.0/RAND_MAX-2;
				cloudvy[i]=rand()*4.0/RAND_MAX-2;
				cloudx[i]=250+cloudvx[i]*50;
				cloudy[i]=250+cloudvy[i]*50;
			}
		}
		ratio=tick*tick/60.0/60.0;
	case STATE::SPREAD:
		for(int i=0;i<10;i++) {
			cloudx[i]+=cloudvx[i];
			cloudy[i]+=cloudvy[i];
		}
		drawClouds(10,cloudx,cloudy,cloudr,cloudtype,ratio);
		break;

	}

	refresh();

}