#include <Windows.h>
#include <cmath>
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
#define WINDOWNAME "image"
HWND hWnd;
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,PSTR szCmdLine, int iCmdShow) {    
    static char szAppName[] = WINDOWNAME;
    MSG    msg ;
    WNDCLASS wndclass ;
	wndclass.style        = CS_HREDRAW | CS_VREDRAW ;
	wndclass.lpfnWndProc  = WndProc ;    
	wndclass.cbClsExtra   = 0 ;   
    wndclass.cbWndExtra   = 0 ;
    wndclass.hInstance    = NULL ;
    wndclass.hIcon        = LoadIcon (NULL, IDI_APPLICATION) ;        
	wndclass.hCursor      = LoadCursor (NULL, IDC_ARROW) ;    
	wndclass.hbrBackground= (HBRUSH) GetStockObject (WHITE_BRUSH) ;    
	wndclass.lpszMenuName = NULL ;
    wndclass.lpszClassName= szAppName ;
    if (!RegisterClass (&wndclass)) {
		MessageBox(NULL,"This program requires Windows NT!",szAppName, MB_ICONERROR) ;
        return 0; 
    }
    hWnd = CreateWindow( szAppName,      // window class name
                   WINDOWNAME,   // window caption
                   WS_SYSMENU,  // window style
                   CW_USEDEFAULT,// initial x position
                   CW_USEDEFAULT,// initial y position
                   500,// initial x size
                   530,// initial y size
                   NULL,                 // parent window handle
				   NULL,            // window menu handle
				   NULL,   // program instance handle
				   NULL) ;      // creation parameters                   
    ShowWindow (hWnd, SW_SHOWNORMAL) ;
    UpdateWindow (hWnd) ;
    while (GetMessage (&msg, NULL, 0, 0)) {
		TranslateMessage (&msg);
		DispatchMessage (&msg) ;
    }
    return 0;
}
#define pix(x,y) (*(p+((y)*500+(x))))

#define PI 3.1415926535897

void translate1(int *data) {
	for (int x=0;x<500;x++) {
		for (int y=0;y<500;y++) {
			int *p=data+(y*500+x);
			double ratio=min(1-pow(fabs(y-250.0),5)/pow(250.0,5),1-pow(fabs(x-250.0),5)/pow(250.0,5));
			int R=*p>>16;
			int G=(*p>>8)&0xff;
			int B=*p&0xff;
			R*=ratio;
			G*=ratio;
			B*=ratio;
			*p=RGB(R,G,B);
		}
	}
}
void translate2(int *data) {
	int *p=new int[500*500];
	for (int x=2;x<500-2;x++)
		for (int y=2;y<500-2;y++) {
			*(p+x+y*500)=0;
			int R=0;
			int G=0;
			int B=0;
			for (int dx=-2;dx<=2;dx++)
				for (int dy=-2;dy<=2;dy++) {
					int *p=data+((dy+y)*500+x+dx);
					int r=*p>>16;
					int g=(*p>>8)&0xff;
					int b=*p&0xff;
					R+=r;
					G+=g;
					B+=b;
				}
			*(p+x+y*500)=RGB(R/25,G/25,B/25);
		}
	memcpy(data,p,500*500*4);
}
#define translate translate2
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_PAINT: {
						HBITMAP hbitmap=(HBITMAP)LoadImage(NULL,"image.bmp",IMAGE_BITMAP,500,500,LR_LOADFROMFILE);
						if (hbitmap) {
							int *bits=new int[500*500]();
							GetBitmapBits(hbitmap,500*500*4,bits);
//							fill(bits);
							translate(bits);
							SetBitmapBits(hbitmap,500*500*4,bits);
						}
						HDC hdc=GetDC(hWnd);
						HDC hdcbuf=CreateCompatibleDC(hdc);
						SelectObject(hdcbuf,hbitmap);
						BitBlt(hdc,0,0,500,500,hdcbuf,0,0,SRCCOPY);
						DeleteDC(hdcbuf);
					}
					break;
	case WM_CLOSE: 
		exit(0);
	}
	return DefWindowProc (hwnd, message, wParam, lParam) ; 
}

