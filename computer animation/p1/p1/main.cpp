#include <math.h>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <cmath>
#include "glut.h"

using namespace std;
float PI=acos(0.0f)*2;
double dis(double dx,double dy) {
//	printf("%lf %lf return %lf\n",dx,dy,sqrt(dx*dx+dy*dy));
	return sqrt(dx*dx+dy*dy);
}
double arc(double dx,double dy) {
	if (fabs(dx)<1e-7)
		if (dy>0)
			return PI/2;
		else
			return PI/2*3;
	if (dy>-1e-7)
		return acos(dx/dis(dx,dy));
	return 2*PI-acos(dx/dis(dx,dy));
}
struct point {
	point(double x,double y):x(x),y(y) {}
	point(){}
	double x,y;
};
vector<point> load(string filename) {
	vector <point> ret;
	ifstream fin(filename.c_str());
	if (!fin) {
		exit(1);
	}
	double a,b;
	while (fin >> a >> b) {
		a/=5;
		b/=5;
		ret.push_back(point(a,b));
	}
	return ret;
}
double t=0;
vector<point> source;
vector<point> dest;
double m[16];
void GetCardinalMatrix(double a1) {
	m[0] = -a1;      m[1] = 2. - a1;  m[2] = a1 - 2.;      m[3] = a1;
	m[4] = 2. * a1;  m[5] = a1 - 3.;  m[6] = 3. - 2 * a1;  m[7] = -a1;
	m[8] = -a1;      m[9] = 0.;       m[10] = a1;          m[11] = 0.;
	m[12] = 0.;      m[13] = 1.;      m[14] = 0.;          m[15] = 0.;   
}
double Matrix(double a, double b, double c, double d, double alpha) {
    double p0, p1, p2, p3;
	p0 = m[0] * a + m[1] * b + m[2] * c + m[3] * d;
    p1 = m[4] * a + m[5] * b + m[6] * c + m[7] * d;
	p2 = m[8] * a + m[9] * b + m[10] * c + m[11] * d;
    p3 = m[12] * a + m[13] * b + m[14] * c + m[15] * d;
    return(p3 + alpha * (p2 + alpha * (p1 + alpha * p0)));
}
vector <point> CubicSpline(vector<point> now) {
	int grain=50;
	double tension=0.5;
	vector <point> res;
	vector <double> alpha;
	vector <point> tcheck;
	res.clear();
	tcheck.clear();
	tcheck.push_back(now[0]);
	for (int j = 0; j < now.size(); ++j) tcheck.push_back(now[j]);
	tcheck.push_back(now[now.size() - 1]);
	alpha.clear();
	for(int i = 0; i < grain; i++) alpha.push_back(((double)i)/grain);
	GetCardinalMatrix(tension);
	for(int i = 0; i < tcheck.size() - 3; i++) {
	    for(int j = 0; j < alpha.size(); j++) {
			point tmp1;
		    tmp1.x = Matrix(tcheck[i].x, tcheck[i+1].x, tcheck[i+2].x, tcheck[i+3].x, alpha[j]);
		    tmp1.y = Matrix(tcheck[i].y, tcheck[i+1].y, tcheck[i+2].y, tcheck[i+3].y, alpha[j]);
			res.push_back(tmp1);
		}
	}
	return res;
	
}
void timerfunc(int value) {
	glClear(GL_COLOR_BUFFER_BIT);
	
	glColor3f(1, 1, 1);
	t+=0.01;
	if (t>1)
		t=0;
	
	vector <point> now;

// 线性插值
	
	for (int i=0;i<source.size();i++) {
		double x=source[i].x*(1-t)+dest[i].x*t;
		double y=source[i].y*(1-t)+dest[i].y*t;
		now.push_back(point(x,y));
	}
	
// 矢量线性插值
	/*
	now.push_back(source[0]);
	
	for (int i=0;i<source.size()-1;i++) {
		double ss=arc(source[i+1].x-source[i].x,source[i+1].y-source[i].y);
		double ds=arc(dest[i+1].x-dest[i].x,dest[i+1].y-dest[i].y);
		double sr=dis(source[i+1].x-source[i].x,source[i+1].y-source[i].y);
		double dr=dis(dest[i+1].x-dest[i].x,dest[i+1].y-dest[i].y);
		//printf("%lf %lf %lf %lf\n",sr,ss,dr,ds);
		double r=sr*(1-t)+dr*t;
		double s=ss*(1-t)+ds*t;
		
		now.push_back(point(now[i].x+r*cos(s),now[i].y+r*sin(s)));
	}
	*/
	glBegin(GL_POLYGON);

// 特征点插值
	now=CubicSpline(now);
	for (int i=0;i<now.size();i++) {
		
		///printf("drawing %lf %lf\n",now[i].x,now[i].y);
		glVertex2f(now[i].x,now[i].y);
		
	}
	glEnd();	
	glutSwapBuffers();
	glutTimerFunc(100,timerfunc,1);
}

void redraw() {
	t=0;
	timerfunc(1);
}

int main (int argc,  char *argv[]) {
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	int windowHandle = glutCreateWindow("Simple GLUT App");
	glutDisplayFunc(redraw);
	source=load("source.txt");
	dest=load("dest.txt");
	glutTimerFunc(1,timerfunc,1);
	glutMainLoop();
	return 0;
}

