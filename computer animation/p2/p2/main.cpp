#include <Windows.h>
#include <cmath>
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
#define WINDOWNAME "image"
HWND hWnd;
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,PSTR szCmdLine, int iCmdShow) {    
    static char szAppName[] = WINDOWNAME;
    MSG    msg ;
    WNDCLASS wndclass ;
	wndclass.style        = CS_HREDRAW | CS_VREDRAW ;
	wndclass.lpfnWndProc  = WndProc ;    
	wndclass.cbClsExtra   = 0 ;   
    wndclass.cbWndExtra   = 0 ;
    wndclass.hInstance    = NULL ;
    wndclass.hIcon        = LoadIcon (NULL, IDI_APPLICATION) ;        
	wndclass.hCursor      = LoadCursor (NULL, IDC_ARROW) ;    
	wndclass.hbrBackground= (HBRUSH) GetStockObject (WHITE_BRUSH) ;    
	wndclass.lpszMenuName = NULL ;
    wndclass.lpszClassName= szAppName ;
    if (!RegisterClass (&wndclass)) {
		MessageBox(NULL,"This program requires Windows NT!",szAppName, MB_ICONERROR) ;
        return 0; 
    }
    hWnd = CreateWindow( szAppName,      // window class name
                   WINDOWNAME,   // window caption
                   WS_SYSMENU,  // window style
                   CW_USEDEFAULT,// initial x position
                   CW_USEDEFAULT,// initial y position
                   500,// initial x size
                   500,// initial y size
                   NULL,                 // parent window handle
				   NULL,            // window menu handle
				   NULL,   // program instance handle
				   NULL) ;      // creation parameters                   
    ShowWindow (hWnd, SW_SHOWNORMAL) ;
    UpdateWindow (hWnd) ;
    while (GetMessage (&msg, NULL, 0, 0)) {
		TranslateMessage (&msg);
		DispatchMessage (&msg) ;
    }
    return 0;
}
#define pix(x,y) (*(p+((y)*500+(x))))

#define PI 3.1415926535897

void translate1(int *data) {
	int *p=new int[500*500];
	
	for (int y=0;y<500;y++) {
		for (int x=0;x<500;x++) {
			int rx=acos(1-x/250.0)/PI*500;
			pix(x,y)=*(data+y*500+rx);
		}
	}
	memcpy(data,p,500*500*4);
}

void translate2(int *data) {
	int *p=new int[500*500];
	
	for (int y=0;y<500;y++) {
		for (int x=0;x<250;x++) {
			int rx=(pow(x+0.0,5)/pow(250+0.0,4)+x)/2;
			pix(x,y)=*(data+y*500+rx);
		}
		for (int x=250;x<500;x++) {
			int rx=500-(pow(500-x+0.0,5)/pow(250+0.0,4)+500-x)/2;
			pix(x,y)=*(data+y*500+rx);
		}
	}
	memcpy(data,p,500*500*4);
}
void translate3(int *data) {
	int *p=new int[500*500];
	
	for (int y=0;y<500;y++) {
		for (int x=0;x<500;x++) {
			int rx=acos(1-x/250.0)/PI*500;
			pix(x,y)=*(data+y*500+rx);
		}
	}
	memcpy(data,p,500*500*4);
	for (int x=0;x<500;x++) {
		for (int y=0;y<500;y++) {
			int ry=acos(1-y/250.0)/PI*500;
			pix(x,y)=*(data+ry*500+x);
		}
	}
	memcpy(data,p,500*500*4);
}
void translate4(int *data) {
	int *p=new int[500*500];
	
	for (int y=0;y<500;y++) {
		for (int x=0;x<250;x++) {
			int rx=(pow(x+0.0,5)/pow(250+0.0,4)+x)/2;
			pix(x,y)=*(data+y*500+rx);
		}
		for (int x=250;x<500;x++) {
			int rx=500-(pow(500-x+0.0,5)/pow(250+0.0,4)+500-x)/2;
			pix(x,y)=*(data+y*500+rx);
		}
	}
	memcpy(data,p,500*500*4);
	for (int x=0;x<500;x++) {
		for (int y=0;y<250;y++) {
			int ry=(pow(y+0.0,5)/pow(250+0.0,4)+y)/2;
			pix(x,y)=*(data+ry*500+x);
		}
		for (int y=250;y<500;y++) {
			int ry=500-(pow(500-y+0.0,5)/pow(250+0.0,4)+500-y)/2;
			pix(x,y)=*(data+ry*500+x);
		}
	}
	memcpy(data,p,500*500*4);
}
void fill(int *data) {
	for (int y=0;y<500;y++)
		for (int x=0;x<500;x++)
			if ((y/10+x/10)%2)
				*(data+y*500+x)=RGB(255,255,255);
			else
				*(data+y*500+x)=RGB(0,0,0);
}
#define translate translate4
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_PAINT: {
						HBITMAP hbitmap=(HBITMAP)LoadImage(NULL,"image.bmp",IMAGE_BITMAP,500,500,LR_LOADFROMFILE);
						if (hbitmap) {
							int *bits=new int[500*500]();
							GetBitmapBits(hbitmap,500*500*4,bits);
//							fill(bits);
							translate(bits);
							SetBitmapBits(hbitmap,500*500*4,bits);
						}
						HDC hdc=GetDC(hWnd);
						HDC hdcbuf=CreateCompatibleDC(hdc);
						SelectObject(hdcbuf,hbitmap);
						BitBlt(hdc,0,0,500,500,hdcbuf,0,0,SRCCOPY);
						DeleteDC(hdcbuf);
					}
					break;
	case WM_CLOSE: 
		exit(0);
	}
	return DefWindowProc (hwnd, message, wParam, lParam) ; 
}

