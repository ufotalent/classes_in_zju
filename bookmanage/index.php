<html>
<?php 
$chosen='General';
$entry='';
include("side.php");
?>
<div id="main-content"> <!-- Main Content Section with everything -->
			
			<noscript> <!-- Show a notification if the user has disabled javascript -->
				<div class="notification error png_bg">
					<div>
						Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
					Download From <a href="http://www.exet.tk">exet.tk</a></div>
				</div>
			</noscript>
			
			<!-- Page Head -->
			<h2>Welcome To Book Administration System</h2>
			<p id="page-intro">Recommended actions</p>
			
			<ul class="shortcut-buttons-set">
				<li><a class="shortcut-button" href="Inquery_Detailed_Inquery"><span>
					<img src="resources/images/icons/paper_content_pencil_48.png" alt="icon" /><br />
					Have a detailed inquery
				</span></a></li>

				<li><a class="shortcut-button" href="Borrow.php"><span>
					<img src="resources/images/icons/pencil_48.png" alt="icon" /><br />
					Borrow a book
				</span></a></li>
				
				<li><a class="shortcut-button" href="Return"><span>
					<img src="resources/images/icons/clock_48.png" alt="icon" /><br />
					Return books
				</span></a></li>
				
				<li><a class="shortcut-button" href="AddCard.php"><span>
					<img src="resources/images/icons/image_add_48.png" alt="icon" /><br />
					Add a library card
				</span></a></li>
				
				<li><a class="shortcut-button" href="SBook.php"><span>
					<img src="resources/images/icons/comment_48.png" alt="icon" /><br />
					Add a new Book
				</span></a></li>
				
			</ul><!-- End .shortcut-buttons-set -->
			
			<div class="clear"></div> <!-- End .clear -->
			
			
			<div class="content-box column-left">
				
				<div class="content-box-header">
					
					<h3>General Status</h3>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab">
					
						<div class="notification success png_bg">
							<a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
				There are totally <?php
$con=mysql_connect("localhost","root","root");
if (!$con)
	die("err");
mysql_query("use bookmanage");
$result=mysql_query("select sum(total_storage) from books");
$row =mysql_fetch_array($result);
if (isset($row[0]))
	echo $row[0];
else
	echo 0;
mysql_close($con);
?> books belonging to the library.
							</div>
						</div>

						<div class="notification success png_bg">
							<a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
<?php
$con=mysql_connect("localhost","root","root");
if (!$con)
	die("err");
mysql_query("use bookmanage");
$result=mysql_query("select sum(storage) from books");
$row =mysql_fetch_array($result);
if (isset($row[0]))
	echo $row[0];
else
	echo 0;
?> books are currently in storage.
							</div>
						</div>

						
					</div> <!-- End #tab3 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
			<div class="content-box column-right closed-box">
				
				<div class="content-box-header"> <!-- Add the class "closed" to the Content box header to have it closed by default -->
					
					<h3>Advanced Actions</h3>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab">
					
						<div class="notification attention png_bg">
							<a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
								<h3>This action is unrecoverable</h3>
<a href="#messages" rel="modal" title="reset">Reset Database</a>

							</div>
						</div>						
					</div> <!-- End #tab3 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			<div class="clear"></div>
			
			
			<!-- Start Notifications -->
			
			<!-- End Notifications -->
			
			<div id="footer">
				<small> <!-- Remove this notice or replace it with whatever you want -->
						&#169; Copyright 2011 Ufotalent | Powered by <a href="http://Ufotalent.co.cc">Ufotalent</a> | <a href="#">Top</a>
				</small>
			</div><!-- End #footer -->
			
		</div> <!-- End #main-content -->

</html>
