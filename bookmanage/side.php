<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?php
session_start();

if ($chosen!='General' && $chosen!='Inquery') {
	if (!isset($_SESSION['adminname'])) {
		echo "<script>window.location =\"Sign In.php\";</script>";
	}
}
?>
 <head>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

		<title>Book Management System</title>

		<!--                       CSS                       -->

		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />

		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />

		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />	

		<!-- Colour Schemes

		Default colour scheme is green. Uncomment prefered stylesheet to use it.

		<link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />

		<link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  

		-->

		<!-- Internet Explorer Fixes Stylesheet -->

		<!--[if lte IE 7]>
			<link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
		<![endif]-->

		<!--                       Javascripts                       -->

		<!-- jQuery -->
		<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>

		<!-- jQuery Configuration -->
		<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>

		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="resources/scripts/facebox.js"></script>

		<!-- jQuery WYSIWYG Plugin -->
		<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>

		<!-- jQuery Datepicker Plugin -->
		<script type="text/javascript" src="resources/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/scripts/jquery.date.js"></script>
		<!--[if IE]><script type="text/javascript" src="resources/scripts/jquery.bgiframe.js"></script><![endif]-->


		<!-- Internet Explorer .png-fix -->

		<!--[if IE 6]>
			<script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('.png_bg, img, li');
</script>
		<![endif]-->

	</head>

	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

		<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

			<h1 id="sidebar-title"><a href="#">Simpla Admin</a></h1>

			<!-- Logo (221px wide) -->
			<a href="index.php"><img id="logo" src="resources/images/logo.png" alt="Main Page" /></a>

			<!-- Sidebar Profile links -->
			<div id="profile-links">
			Hello, <?php if (isset($_SESSION["adminname"])) echo $_SESSION["adminname"]; else echo "Guest"; ?> <br />
				<br />
<?php if (isset($_SESSION["adminname"])) echo '<a  href="Sign Out.php" title="Sign Out">Sign Out</a>'; else echo '<a  href="Sign In.php" title="Sign In">Sign In</a>'; ?>
			</div>        

<div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->
				
				<h3>Comfirm your authority</h3>
			 <form action="resetdb.php" method="post">
				<p>
					<strong>Administrator ID</strong><br />
					<input class="text-input medium-input datepicker" type="text" id="small-input" name="aid" />
				</p>
			 
				<p>
					<strong>Password</strong><br />
					<input class="text-input medium-input datepicker" type="password" id="small-input" name="passwd" />
				</p>
			 
					
						
						<input class="button" type="submit" value="Reset" />
						
					</fieldset>
					
				</form>
				
			</div> <!-- End #messages -->
			
			<ul id="main-nav">  <!-- Accordion Menu -->

				<li>
				<a  class="nav-top-item <?php if ($chosen=='General') echo 'current'; ?>"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						General
					</a>       
					<ul>
					<li><a <?php if ($entry=='') echo "class = 'current' "?> href="index.php">Home</a></li>
					</ul>

				</li>

				<li> 
					<a  class="nav-top-item <?php if ($chosen=='Inquery') echo 'current'; ?>"> <!-- Add the class "current" to current menu item -->
					Inquery
					</a>
					<ul>
						<li><a <?php if ($entry=='By name') echo "class = 'current'" ?> href="Inquery_By_name.php">By name</a></li> <!-- Add class "current" to sub menu items also -->
						<li><a <?php if ($entry=='By author') echo "class = 'current'" ?> href="Inquery_By_author.php">By author</a></li>
						<li><a <?php if ($entry=='By type') echo "class = 'current'" ?> href="Inquery_By_type.php">By type</a></li>
						<li><a <?php if ($entry=='Detailed Inquery') echo "class = 'current'" ?> href="Inquery_Detailed_Inquery.php">Detailed Inquery</a></li>
					</ul>
				</li>

				<li>
				<a  class="nav-top-item <?php if ($chosen=='BR') echo "current"; ?> ">
						Borrow and Return
					</a>
					<ul>
					<li><a <?php if ($entry=='Borrow') echo "class = 'current' "?> href="Borrow.php">Borrow</a></li>
						<li><a <?php if ($entry=='Return') echo "class = 'current' "?> href="Return.php">Return</a></li>
					</ul>

				</li>

				<li>
				<a  class="nav-top-item <?php if ($chosen=='Card') echo "current"?>">
						Library Cards
					</a>
					<ul>
						<li><a <?php if ($entry=='addcard') echo "class = 'current' "?> href="AddCard.php">Add a Library Card</a></li>
						<li><a <?php if ($entry=='removecard') echo "class = 'current' "?>href="RemoveCard.php">Remove a Library Card</a></li>
					</ul>
				</li>

				<li>
					<a  class="nav-top-item <?php if ($chosen=='Incoming') echo "current"?>">
						Incoming Books
					</a>
					<ul>
						<li><a <?php if ($entry=='sbook') echo "class = 'current' "?> href="SBook.php">Single Book</a></li>
						<li><a <?php if ($entry=='bbook') echo "class = 'current' "?> href="BBook.php">Batch of Books</a></li>
					</ul>
				</li>      

			</ul> <!-- End #main-nav -->

		</div></div> <!-- End #sidebar -->


	</div></body>


<!-- Download From www.exet.tk-->

