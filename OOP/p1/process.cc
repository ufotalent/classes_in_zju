#include "process.h"
#include "students.h"
#include <map>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
using namespace std;

#define NO_SCORE -1										
map <string, int> CourseIndex;      					// use a map to create index from course name to its numeric index
map <string, int> StudentIndex;							// use a map to create index from student name to its numeric index


vector <Student> Students;								// vector used to store students information
vector <string> Coursenames;							// vector used to store course names


bool insertScore (string sid, string cname, int score) {		// insert a score of student with id=sid and course name =cname. Returns whether the insertion is successful
	map <string, int> ::iterator it = StudentIndex.find(sid);	// find the index of student by looking up sid in the map StudentIndex

	if (it == StudentIndex.end()) {								// show error if not found
		cerr << "Student ID not found" << endl;
		return false;
	} 
	int studentindex = StudentIndex[sid];		
	if (CourseIndex.find(cname) == CourseIndex.end()) {			// find the index of student by looking up cname in the map CourseIndex
		cerr << "Course name not found" << endl;				// show error if not found
		return false;
	} 
	int	courseindex = CourseIndex[cname];						
	Students[studentindex].setScore(courseindex,score);			// update the score
	return true;
}

bool insertStudent (string sid,string sname) {					// create the information of a new student. Return value indicate where the execution is successful
	if (StudentIndex.find(sid) != StudentIndex.end()) {			// look up the sid in map
		cerr << "such sid already exists" << endl;				// show error if there exists sid
		return false;
	}
	int newindex = StudentIndex.size();							// the index of the new student is the number of student now
	StudentIndex[sid] = newindex;								// create index
	Students.push_back(Student(sid,sname));						// push into the vector Students with a new constructed Studente
	return true;e
}

bool insertCourse (string cname) {								// insert the course name into vector 
	if (CourseIndex.find(cname) != CourseIndex.end()) {			// assert the course name doesn't exist before
		cerr << "the course already exists" << endl;
		return false;
	}
	int newindex = CourseIndex.size();							// the index of the new course is the number of course now
	CourseIndex[cname] = newindex;								// create index
	Coursenames.push_back(cname);
	for (int i = 0; i < StudentIndex.size(); i++) {				// update the length of vector score for each student
		Students[i].updateSize();
	}
	return true;
}

void showTable() {												// the function displays the table containing all informaion as well as average score

	cout << "---------------------------query---------------------------------" << endl;
// displays the header of table
	cout << setw(4) << "s\\n" << setw(12) << "id" << setw(10) << "name";
	for (int i = 0; i < Coursenames.size(); i++)
		cout << setw(Coursenames[i].length() + 3) << Coursenames[i] ;
	cout << setw(9) << "average";
	cout << endl << endl;
// output the information of every student
	for (int i = 0; i < Students.size(); i++) {
// initialize the counter for calculating the average score
		int count = 0;
		double sum = 0;

// display the student's basic information
		cout << setw(4) << i+1 << setw(12) << Students[i].getsid() << setw(10) << Students[i].getsname();
// output course score
		for (int j = 0; j < Coursenames.size(); j++) {
			if (Students[i].getScore(j) != NO_SCORE) {
				cout<< setw(Coursenames[j].length() + 3) << Students[i].getScore(j);
				sum += Students[i].getScore(j);
				count++;
			}
			else
				cout<< setw(Coursenames[j].length() + 3) << "";

		}
// print out the average score 
		if (count == 0)
			cout << setw(9) << "n\\a";
		else
			cout << setw(9) << (int) (sum / count);
		cout << endl;
	}

	cout << endl;
	cout << setw(12) << "average" << setw(10) << "" << setw(4) << "" ;
// print the average line in the bottom
	int totalcount = 0;
	double totalsum = 0;
	for (int i = 0; i < CourseIndex.size(); i++) {
		int count = 0;
		double sum = 0;
		for (int j = 0; j < Students.size(); j++) {
			if (Students[j].getScore(i) != NO_SCORE) {
				count++;
				sum += Students[j].getScore(i);
			}
		}
		if (count == 0)
			cout << setw(Coursenames[i].length() + 3) << "n\\a";
		else {
			cout << setw(Coursenames[i].length() + 3) << (int) (sum / count);
			totalcount += count;
			totalsum += sum;
		}
	}
	if (totalcount)
		cout << setw(9) << (int) (totalsum / totalcount) << endl;
	else
		cout << setw(9) << "n\\a" <<endl;

	cout << "------------------------end query--------------------------------" << endl;
}
