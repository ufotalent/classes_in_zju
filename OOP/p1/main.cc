//////////////////////////////////////////////////////////////////////
//		Author		: Xinyi WAN
//		SID			: 3090101754
//		Date		: 3/15/2011
//		Department	: CS, ZJU
//

#include <iostream>
#include <string>
#include <strstream>
#include "process.h"
using namespace std;

void showError() {              // function that displays error message while parsing the request
	cerr << "Syntax Error" << endl;
}

//the data of help menu
string menu[]={"--------------syntax------------","insert student %student_id %student_name","insert course %course_name","insert score %student_id %course_name %score","query","exit","help"};

void help() {					// print help message
	for (int i = 0; i < 7; i++)
		cout << menu[i] << endl;
}

int main() {
	string cmdline;				// string used to store the header of command
	char buf[200];				// the buffer of input
	help();
	while (1) {
		cout << "\>";			
		cin.getline(buf,200);	// read one line from cin  
		cout <<endl;
		strstream str;			// the string stream used for parsing the request
		str << buf;				// push the content in buffer into string stream
		str >> cmdline;			// get the header of the command
		if (cmdline == "exit") 	// deal with "exit"
			return 0;
		if (cmdline == "insert") {									// for "insert" command
			string type;
			str >> type;
			if (type == "student") {								// if the insertion is student 
				string id,name;
				if (!(str >> id && str >> name)) {					// read and assert the student id and name to insert 
					showError();
				} else	insertStudent(id,name);						// call insertStudent
			} else if (type == "score") {							// if the insersion is score
				string sid,cname;						
				int score;	
				if (!(str >> sid && str >>cname && str >> score)) 	// read and assert the student id, course name and the score of that course
					showError();
				else	
					insertScore(sid,cname,score);					// call insertScore
			} else if (type == "course") {							// if the insersion is course
				string cname;
				if (!(str >> cname))								// read and assert the course name to be added
					showError();
				else
					insertCourse(cname);							// call insertCourse
			} else showError();
		} else if (cmdline == "query") {							// for query command
			showTable();						
		} else if (cmdline == "help") {								// print help message
			help();
		} else showError();											// if the head of command is none of these above, display error message
	}
}

