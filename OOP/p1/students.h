#include <vector>
using namespace std;

class Student {											// class of student
private:
	string sid;											// the student id
	string sname;										// the name of student
	vector <int> Score;									// vector used to store the student's course scores
public:
	Student(string newsid, string newsname):sid(newsid),sname(newsname) {		// constructor, initialize the length of vector score
		Score.resize(CourseIndex.size(),NO_SCORE);
	}

	string getsid() { return sid; }

	string getsname() { return sname; }

	void updateSize() {
		Score.resize(CourseIndex.size(),NO_SCORE);		// function used to resize the vector of score. Used when inserting new courses.
	}
	int getScore(int courseID) {						// get the score 
		return Score[courseID];
	}
	void setScore(int courseID, int score) {
		Score[courseID]=score;
	}

};
