//////////////////////////////////////////////////////////////////////
//		Author		: Xinyi WAN
//		SID			: 3090101754
//		Date		: 3/15/2011
//		Department	: CS, ZJU
//

#include <iostream>
#include <string>
#include <vector>
using namespace std;

typedef vector<string> MENU;

MENU menu={"1 query","2 insert","quit"};
int main() {
	string cmdline;
	while (1) {
		for (MENU::iterator it=menu.begin() ; it!=menu.end();it++) {
			cout<<*it<<endl;
		}
		cout << "Select operation" << endl;
		cin >> cmdline;
		switch (cmdline) {
			case "quit":
				return 0;
		}
	}
}
