#include "stash.h"
#include <iostream>
using namespace std;

int main(){

	stash<int> data(-1);
	stash<int> ndata(2);
	data.put(1);
	data.put(2);
	data.put(3);
	ndata=data;
	for (int i = 0; i <= data.count(); i++)
		cout << data.get(i);
	cout << endl;
	int h = data.first();
	while (h != data.invalid) {
		cout << h;
		h=data.next(h);
	}
	cout << data.next(-1);
	cout <<endl;
	{
		ndata.remove(1);
		for (int i = 0; i <= ndata.count(); i++)
			cout << ndata.get(i);
		cout << endl;
		int h = ndata.first();
		while (h != ndata.invalid) {
			cout << h;
			h=ndata.next(h);
		}
		cout << ndata.next(-1);
		cout <<endl;
	}
	{
		ndata.remove(2);
		for (int i = 0; i <= ndata.count(); i++)
			cout << ndata.get(i);
		cout << endl;
		int h = ndata.first();
		while (h != ndata.invalid) {
			cout << h;
			h=ndata.next(h);
		}
		cout << ndata.next(-1);
		cout <<endl;
	}
	{
		ndata.remove(3);
		for (int i = 0; i <= ndata.count(); i++)
			cout << ndata.get(i);
		cout << endl;
		int h = ndata.first();
		while (h != ndata.invalid) {
			cout << h;
			h=ndata.next(h);
		}
		cout << ndata.next(-1);
		cout <<endl;
	}
}