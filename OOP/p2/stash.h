#ifndef _H_STASH_
#define _H_STASH_

#include <iostream>
using namespace std;
template <class T> class stash {
private:
	typedef struct linknode{
		T value;
		linknode * next;
		linknode() {
			next = NULL;			
		}
	} *pnode;

	pnode head;			//the head of list
	int buf_count;			//buffer for count
public:
	T invalid;
	stash(const T & invalidflag):head(NULL),buf_count(0) {
		this->invalid=invalidflag;
	}

	stash(const stash &val) {
		this->buf_count = val.buf_count;
		this->invalid = val.invalid;
		pnode tempf,tempt;
		tempf = val.head;
		tempt = NULL;
		while (tempf) {
			if (tempt == NULL) {
				this->head = tempt = new linknode();
				tempt->value = tempf->value;
				tempf = tempf->next;
			} else {
				tempt->next = new linknode();
				tempt = tempt->next;
				tempt->value = tempf->value;
				tempf = tempf->next;
			}
		}

	}
	
	stash& operator = (const stash &val) {
		clear();
		this->buf_count = val.buf_count;
		this->invalid = val.invalid;
		pnode tempf,tempt;
		tempf = val.head;
		tempt = NULL;
		while (tempf) {
			if (tempt == NULL) {
				this->head = tempt = new linknode();
				tempt->value = tempf->value;
				tempf = tempf->next;
			} else {
				tempt->next = new linknode();
				tempt = tempt->next;
				tempt->value = tempf->value;
				tempf = tempf->next;
			}
		}
		return *this;
		
	}

	void put(const T &val) {					//new node will be inserted at the head
		pnode oldnode = this->head;
		this->head = new linknode();
		this->head->value = val;
		this->head->next = oldnode;
		buf_count++;
	}

	bool remove(const T &val) {							// all matching items will be deleted
		if (this->head == NULL)
			return false;
		bool result = false;
		while (this->head && this->head->value == val) {
			pnode newhead = this->head->next;
			delete(this->head);
			this->head = newhead;
			result = true;
			buf_count--;
		}
		if (this->head == NULL)
			return result;
		pnode temp = this->head;
		while (temp->next) {
			while (temp->next && temp->next->value == val) {
				pnode newnext = temp->next->next;
				delete(temp->next);
				temp->next=newnext;
				result=true;
				buf_count--;
			}
			if (temp->next == NULL)
				break;
			temp=temp->next;

		}
		return result;
	}

	int count() {
		return buf_count;
	}

	bool has(const T &val) {
		pnode temp = this->head;
		while (temp) {
			if (temp->value == val) 
				return true;
			temp = temp->next;
		}
		return false;
	}

	T& first() {
		if (head)
			return head->value;
		else
			return invalid;
	}

	T& next(const T & val) {
		pnode temp = this->head;
		while (temp) {
			if (temp->value == val) {
				if (temp->next)
					return temp->next->value;
				else
					return invalid;
			}
			temp = temp->next;
		}
		return invalid;

	}

	T& get(const int & index) {
		if (index >= buf_count)
			return invalid;
		pnode temp = this->head;
		for (int i = 1; i <=  index; i++)
			temp = temp->next;
		return temp->value;

	}

	void clear() {
		this->buf_count=0;
		pnode temp=this->head;
		while (temp) {
			pnode next = temp->next;
			delete(temp);
			temp = next;
		}
		this->head = NULL;

	}

	void print() {
		pnode temp = this->head;
		while (temp) {
			cout << temp->value << " ";
			temp = temp ->next;
		}
		cout <<endl;
	}
};



#endif
