#ifndef _DIARY_H_
#define _DIARY_H

#include <map>
#include <string>
#include <vector>
using namespace std;

class diary {
private:
	map <string,string> datafield;	
	string filename;
public:
	diary(const string& filename);
	~diary();
	void save();
	void insert(const string& date,const string& content);
	bool erase(const string& date);
	const string get(const string& date);
	vector <string> getDateList();
};
#endif
