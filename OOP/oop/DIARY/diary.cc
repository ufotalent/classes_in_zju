#include "diary.h"
#include <fstream>
#include <strstream>


using namespace std;
diary::diary(const string& filename):filename(filename),datafield() {
	ifstream fin(filename.c_str());
	string buffer;
	while (getline(fin,buffer)) {
		strstream str;
		str<< buffer;
		string date,content;
		str >> date >> content;
		insert(date,content);
	}
	fin.close();
}
diary::~diary() {
	save();
	datafield.clear();
}
void diary::save(){
	ofstream fout(filename.c_str());
	for (map<string,string>::iterator it=datafield.begin();it!=datafield.end();it++) {
		fout << it->first << " " << it->second << endl;
	}
	fout.close();
}
void diary::insert(const string& date,const string& content) {
	datafield[date]=content;
}

bool diary::erase(const string& date) {
	map<string,string>::iterator it;
	if ((it = datafield.find(date)) == datafield.end())
		return false;
	datafield.erase(it);
	return true;
}

const string diary::get(const string& date) {
	if (datafield.find(date) == datafield.end())
		return "";
	return datafield[date];
}

vector <string> diary::getDateList() {
	vector <string> list;
	for (map<string,string>::iterator it=datafield.begin();it!=datafield.end();it++) {
		list.insert(list.end(),it->first);
	}
	return list;
}
