#include "fraction.h"
#include <iostream>
#include <strstream>
#include <string>
using namespace std;
int abs(int s) {
	if (s>0)
		return s;
	return -s;
}
int gcd(int s,int t) {
	if (t==0)
		return s;
	else
		return gcd(t,s%t);
}
void Fraction::reduce(Fraction& data) {
	if (data.numerator == 0) {
		data.denominator = 1;
		return ;
	}
	if (data.denominator == 0) {
		data.numerator = 1;
		return ;
	}
	if (data.denominator < 0) {
		data.denominator = -data.denominator;
		data.numerator = -data.numerator;
	}

	int div=gcd(abs(data.denominator),abs(data.numerator));
	data.denominator/=div;
	data.numerator/=div;
}
Fraction::Fraction():numerator(0),denominator(1) {

}

Fraction::Fraction(int numerator,int denominator):numerator(numerator),denominator(denominator) {

}

Fraction::Fraction(const Fraction& source) {
	numerator=source.numerator;
	denominator=source.denominator;
}

const Fraction Fraction::operator +(const Fraction& rhs) const {
	Fraction ret;
	ret.numerator=this->numerator*rhs.denominator + this->denominator*rhs.numerator;
	ret.denominator=this->denominator*rhs.denominator;
	reduce(ret);
	return ret;
}

const Fraction Fraction::operator -(const Fraction& rhs) const {
	Fraction ret;
	ret.numerator=this->numerator*rhs.denominator - this->denominator*rhs.numerator;
	ret.denominator=this->denominator*rhs.denominator;
	reduce(ret);
	return ret;
}

const Fraction Fraction::operator *(const Fraction& rhs) const {
	Fraction ret;
	ret.numerator=this->numerator*rhs.numerator;
	ret.denominator=this->denominator*rhs.denominator;
	reduce(ret);
	return ret;
}

const Fraction Fraction::operator /(const Fraction& rhs) const {
	Fraction ret;
	ret.numerator=this->numerator*rhs.denominator;
	ret.denominator=this->denominator*rhs.numerator;
	reduce(ret);
	return ret;
}

bool Fraction::operator <(const Fraction& rhs) const {
	if (this->numerator*rhs.denominator - this->denominator*rhs.numerator < 0)
		return true;
	return false;
}

bool Fraction::operator <=(const Fraction& rhs) const {
	return (*this<rhs) || (*this==rhs);
}

bool Fraction::operator ==(const Fraction& rhs) const {
	return (this->numerator*rhs.denominator == this->denominator*rhs.numerator);
}

bool Fraction::operator !=(const Fraction& rhs) const {
	return !(*this==rhs);
}

bool Fraction::operator >=(const Fraction& rhs) const {
	return !(*this<rhs);
}

bool Fraction::operator >(const Fraction& rhs) const {
	return !(*this<=rhs);
}

Fraction::operator double() const {
	return double(numerator)/denominator;
}

const string Fraction::toString() const {
	strstream str;
	str << numerator << "/" << denominator;
	string ret;
	str >> ret;
	return ret;
}

ostream& operator <<(ostream& os, const Fraction& rhs){
	os << rhs.toString();
	return os;
}

istream& operator >>(istream& os, Fraction& rhs){
	char voidchar;
	os >> rhs.numerator >> voidchar >> rhs.denominator;
	return os;
}
