#ifndef		_FRACTION_H_
#define 	_FRACTION_H_
#include <string>
using namespace std;
class Fraction {
private:
	int numerator;
	int denominator;
	static void reduce(Fraction& data);
public:
	Fraction();
	Fraction(int numerator,int denominator);
	Fraction(const Fraction& source);
	const Fraction operator +(const Fraction& rhs) const;
	const Fraction operator -(const Fraction& rhs) const;
	const Fraction operator *(const Fraction& rhs) const;
	const Fraction operator /(const Fraction& rhs) const;

	bool operator <(const Fraction& rhs) const;
	bool operator <=(const Fraction& rhs) const;
	bool operator ==(const Fraction& rhs) const;
	bool operator !=(const Fraction& rhs) const;
	bool operator >=(const Fraction& rhs) const;
	bool operator >(const Fraction& rhs) const;

	operator double() const;
	const string toString() const;
	friend ostream& operator <<(ostream& os, const Fraction& rhs);
	friend istream& operator >>(istream& os, Fraction& rhs);
};
#endif
