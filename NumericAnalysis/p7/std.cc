#include <stdio.h>
#include <math.h>
#define MAX_m 200
#define MAX_n 5
 
double f1(double x)
{
    return sin(x);
}
 
double f2(double x)
{
    return exp(x);
}
 
int OPA( double (*f)(double t), int m, double x[], double w[], double c[], double *eps );
 
void print_results( int n, double c[], double eps)
{   
    int i;
 
    printf("%d\n", n);
    for (i=0; i<=n; i++)
       printf("%8.4e ", c[i]);
    printf("\n");
    printf("error = %6.2e\n", eps);
    printf("\n");
}
 
int main()
{
    
    int m, i, n;
    double x[MAX_m], w[MAX_m], c[MAX_n+1], eps;
 
    m = 90;
    for (i=0; i<m; i++) {
       x[i] = 3.1415926535897932 * (double)(i+1) / 180.0;
       w[i] = 1.0;
    }
    eps = 0.001;
    n = OPA(f1, m, x, w, c, &eps);
    print_results(n, c, eps);
 
    m = 200;
    for (i=0; i<m; i++) {
       x[i] = 0.01*(double)i;
       w[i] = 1.0;
    }
    eps = 0.001;
    n = OPA(f2, m, x, w, c, &eps);
    print_results(n, c, eps);
 
    return 0;
}



#include <cstring>
#include <cassert>
double GetSingle(int n, double hs[], double x) {
    double ret = 0;
    for (int j = n; j >= 0; --j) {
        ret *= x;
        ret += hs[j];
    }       
    return ret;
}    
int Get(int n, double hs[], double x[], double ret[], int m) {
    for (int j = 0; j < m; ++j) ret[j] = GetSingle(n, hs, x[j]);
    return 0;    
} 
double Cal(int m, double s1[], double s2[], double w[]) {
    double ret = 0;
    for (int j = 0; j <= m; ++j) ret += w[j] * s1[j] * s2[j];
    return ret;
}       

int OPA( double (*f)(double t), int m, double x[], double w[], double P[], double *TOL) {
    double fa0[MAX_n + 10], fa1[MAX_n + 10], fa2[MAX_n + 10], faT[MAX_n + 10];
    double a[MAX_n + 10], B[MAX_n + 10], C[MAX_n + 10];
    double y[MAX_m + 10], tmp[MAX_m + 10], tmp1[MAX_m + 10];
    double err = 0;
    memset(fa0, 0, sizeof(fa0));
    memset(fa1, 0, sizeof(fa1));
    memset(fa2, 0, sizeof(fa2));
    memset(faT, 0, sizeof(faT));
    memset(y, 0, sizeof(y));
    memset(a, 0, sizeof(a));
    memset(B, 0, sizeof(B));
    memset(C, 0, sizeof(C));
    memset(tmp, 0, sizeof(tmp));
    memset(tmp1, 0, sizeof(tmp1));
    memset(P, 0, sizeof(P));
      
    for (int j = 0; j < m; ++j) y[j] = f(x[j]);
    // Step 1
    memset(fa0, 0, sizeof(fa0));
    fa0[0] = 1;
    Get(MAX_n, fa0, x, tmp, m);
    a[0] = Cal(m, tmp, y, w) / Cal(m, tmp, tmp, w);
    for (int j = 0; j <= MAX_n; ++j) P[j] = a[0] * fa0[j];
    err = Cal(m, y, y, w) - a[0] * Cal(m, tmp, y, w);
    // Step 2
    faT[0] = 0; for (int j = 1; j <= MAX_n; ++j) faT[j] = fa0[j - 1];
    Get(MAX_n, faT, x, tmp1, m);
    B[1] = Cal(m, tmp, tmp1, w) / Cal(m, tmp, tmp, w);
    
    memset(fa1, 0, sizeof(fa1));
    fa1[0] = -B[1];
    fa1[1] = 1;
    
    Get(MAX_n, fa1, x, tmp, m);
    a[1] = Cal(m, tmp, y, w) / Cal(m, tmp, tmp, w);
    
    for (int j = 0; j <= MAX_n; ++j) P[j] += a[1] * fa1[j];
    err -= a[1] * Cal(m, tmp, y, w);
    // Step 3

    int k = 1;
    while(k < MAX_n && (fabs(err) > *TOL)) {
        // Step 5
        ++k;
        // step 6
        faT[0] = 0; for (int j = 1; j <= MAX_n; ++j) faT[j] = fa1[j - 1];
        Get(MAX_n, fa1, x, tmp, m);
        Get(MAX_n, faT, x, tmp1, m);
        B[k] = Cal(m, tmp1, tmp, w) / Cal(m, tmp, tmp, w);
        Get(MAX_n, fa0, x, tmp, m);
        C[k] = Cal(m, tmp1, tmp, w) / Cal(m, tmp, tmp, w);
        memset(fa2, 0, sizeof(fa2));
        for (int j = 0; j <= MAX_n; ++j) fa2[j] = -C[k] * fa0[j] -B[k] * fa1[j];
        for (int j = 1; j <= MAX_n; ++j) fa2[j] += fa1[j-1];
        Get(MAX_n, fa2, x, tmp, m);
        a[k] = Cal(m, tmp, y, w) / Cal(m, tmp, tmp, w);
        for (int j = 0; j <= MAX_n; ++j) P[j] += a[k] * fa2[j];
        err -= a[k] * Cal(m, tmp, y, w);
        // Step 7
        memcpy(fa0, fa1, sizeof(fa1));
        memcpy(fa1, fa2, sizeof(fa2));
    }    
    *TOL = err;
    return k;
}    


