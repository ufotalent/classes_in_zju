#include <stdio.h>
#include <math.h>
#define MAX_m 200
#define MAX_n 5
 
double f1(double x)
{
    return sin(x);
}
 
double f2(double x)
{
    return exp(x);
}
 
int OPA( double (*f)(double t), int m, double x[], double w[],
double c[], double *eps );
 
void print_results( int n, double c[], double eps)
{  
    int i;
 
    printf("%d\n", n);
    for (i=0; i<=n; i++)
       printf("%8.4e ", c[i]);
    printf("\n");
    printf("error = %6.2e\n", eps);
    printf("\n");
}
 
int main()
{
    int m, i, n;
    double x[MAX_m], w[MAX_m], c[MAX_n+1], eps;
 
    m = 90;
    for (i=0; i<m; i++) {
       x[i] = 3.1415926535897932 * (double)(i+1) / 180.0;
       w[i] = 1.0;
    }
    eps = 0.001;
    n = OPA(f1, m, x, w, c, &eps);
    print_results(n, c, eps);
 
    m = 200;
    for (i=0; i<m; i++) {
       x[i] = 0.01*(double)i;
       w[i] = 1.0;
    }
    eps = 0.001;
    n = OPA(f2, m, x, w, c, &eps);
    print_results(n, c, eps);
 
    return 0;
}



#include <cstring>
double x[MAX_m];
double w[MAX_m];
double m;
struct vector{
	double values[MAX_m];
	double& operator[](const int index){
		return values[index];
	}	
	vector() {
		memset(values,0,sizeof(values));
	}
};

double operator *(vector a,vector b) {
	double res=0;
	for (int i=0;i<m;i++)
		res+=a[i]*b[i]*w[i];
	return res;
}
struct function {
	double co[MAX_n+10];
	function() {
		memset(co,0,sizeof(co));
	}
	vector get() {
		vector res;
		for (int i=0;i<m;i++) {
			double xx=x[i];
			double val=0;
			for (int j=MAX_n;j>=0;j--)
				val=val*xx+co[j];			
			res[i]=val;
		}
		return res;
	}
	function promote() {
		function ret;
		for (int i=1;i<=MAX_n;i++)
			ret.co[i]=this->co[i-1];
		return ret;
	}
};
function operator* (const function &a,double scale) {
	function ret(a);
	for (int i=0;i<=MAX_n;i++)
		ret.co[i]*=scale;
	return ret;
}
function operator+ (const function &a,const function &b) {
	function ret;
	for (int i=0;i<=MAX_n;i++)
		ret.co[i]=a.co[i]+b.co[i];
	return ret;
}
int OPA( double (*f)(double t), int m, double x[], double w[], double c[], double *eps ) {
	double TOL=*eps;
	::m=m;
	memcpy(::x,x,sizeof(::x));
	memcpy(::w,w,sizeof(::w));
	double a[MAX_n+10];
	double B[MAX_n+10];
	double C[MAX_n+10];
	memset(a,0,sizeof(a));
	memset(B,0,sizeof(B));
	memset(C,0,sizeof(C));
	vector y;
	for (int i=0;i<m;i++) 
		y[i]=f(x[i]);
	function f0;
	f0.co[0]=1;
	a[0]=(f0.get()*y)/(f0.get()*f0.get());
	function P=f0*a[0];
	double err=y*y-a[0]*(f0.get()*y);
	
	B[1]=(f0.promote().get()*f0.get())/(f0.get()*f0.get());
	function f1,f2;
	f1.co[1]=1;
	f1.co[0]=-B[1];
	a[1]=(f1.get()*y)/(f1.get()*f1.get());

	P=P+f1*a[1];
	err-=a[1]*(f1.get()*y);
	int k=1;
	while (k<MAX_n && fabs(err)>TOL) {
		k++;
		B[k]=(f1.promote().get()*f1.get())/(f1.get()*f1.get());
		C[k]=(f1.promote().get()*f0.get())/(f0.get()*f0.get());
		f2=f1.promote()+f1*(-B[k])+f0*(-C[k]);
		a[k]=(f2.get()*y)/(f2.get()*f2.get());
		P=P+f2*a[k];
		err-=a[k]*(f2.get()*y);

		f0=f1;
		f1=f2;
	}
	for (int i=0;i<=MAX_n;i++)
		c[i]=P.co[i];
	*eps=err;
	return k;

}
