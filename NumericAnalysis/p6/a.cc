#include <stdio.h>
 
#define MAX_N 20
 
void Cubic_Spline(int n, double x[], double f[], int Type, double s0, double sn,
double a[], double b[], double c[], double d[]);
 
double S( double t, double Fmax,
            int n, double x[], double a[], double b[], double c[], double d[] );
 
int main()
{
    int n, Type, m, i;
    double x[MAX_N], f[MAX_N], a[MAX_N], b[MAX_N], c[MAX_N], d[MAX_N];
    double s0, sn, Fmax, t0, tm, h, t;
 
    while (scanf("%d", &n) != EOF) {
       for (i=0; i<=n; i++)
           scanf("%lf", &x[i]);
       for (i=0; i<=n; i++)
           scanf("%lf", &f[i]);
       scanf("%d %lf %lf %lf", &Type, &s0, &sn, &Fmax);
 
       Cubic_Spline(n, x, f, Type, s0, sn, a, b, c, d);
       for (i=1; i<=n; i++)
           printf("%12.8e %12.8e %12.8e %12.8e \n", a[i], b[i], c[i], d[i]);
 
       scanf("%lf %lf %d", &t0, &tm, &m);
       h = (tm-t0)/(double)m;
       for (i=0; i<=m; i++) {
           t = t0+h*(double)i;
           printf("f(%12.8e) = %12.8e\n", t, S(t, Fmax, n, x, a, b, c, d));
       }
       printf("\n");
    }
 
    return 0;
}
void natural(int n, double x[], double f[], double s0, double sn, double a[], double b[], double c[], double d[]) {
	double h[MAX_N];
	double l[MAX_N];
	double u[MAX_N];
	double z[MAX_N];	
	double alpha[MAX_N];
	for (int i=0;i<=n;i++)
		a[i]=f[i];	
	for (int i=0;i<n;i++)
		h[i]=x[i+1]-x[i];
	for (int i=1;i<n;i++)
		alpha[i]=3/h[i]*(a[i+1]-a[i])-3/h[i-1]*(a[i]-a[i-1]);
	l[0]=1;
	u[0]=0;
 	z[0]=s0/2;
	for (int i=1;i<n;i++) {
		l[i]=2*(x[i+1]-x[i-1])-h[i-1]*u[i-1];
		u[i]=h[i]/l[i];
		z[i]=(alpha[i]-h[i-1]*z[i-1])/l[i];
	}
	l[n]=1;
	z[n]=sn/2;
	c[n]=0;
	for (int j=n-1;j>=0;j--) {
		c[j]=z[j]-u[j]*c[j+1];
		b[j]=(a[j+1]-a[j])/h[j]-h[j]*(c[j+1]+2*c[j])/3;
		d[j]=(c[j+1]-c[j])/(3*h[j]);
	}


	for (int i=n+1;i>=1;i--) {
		a[i]=a[i-1];
		b[i]=b[i-1];
		c[i]=c[i-1];
		d[i]=d[i-1];
	}
}
void clamped(int n, double x[], double f[], double FPO, double FPN, double a[], double b[], double c[], double d[]) {
	double h[MAX_N];
	double l[MAX_N];
	double u[MAX_N];
	double z[MAX_N];	
	double alpha[MAX_N];
	for (int i=0;i<=n;i++)
		a[i]=f[i];
	for (int i=0;i<n;i++)
		h[i]=x[i+1]-x[i];
	alpha[0]=3*(a[1]-a[0])/h[0]-3*FPO;
	alpha[n]=3*FPN-3*(a[n]-a[n-1])/h[n-1];
	for (int i=1;i<n;i++)
		alpha[i]=3/h[i]*(a[i+1]-a[i])-3/h[i-1]*(a[i]-a[i-1]);
	l[0]=2*h[0];
	u[0]=0.5;
 	z[0]=alpha[0]/l[0];

	for (int i=1;i<n;i++) {
		l[i]=2*(x[i+1]-x[i-1])-h[i-1]*u[i-1];
		u[i]=h[i]/l[i];
		z[i]=(alpha[i]-h[i-1]*z[i-1])/l[i];
	}
	l[n]=h[n-1]*(2-u[n-1]);
	z[n]=(alpha[n]-h[n-1]*z[n-1])/l[n];
	c[n]=z[n];
	for (int j=n-1;j>=0;j--) {
		c[j]=z[j]-u[j]*c[j+1];
		b[j]=(a[j+1]-a[j])/h[j]-h[j]*(c[j+1]+2*c[j])/3;
		d[j]=(c[j+1]-c[j])/(3*h[j]);
	}



	for (int i=n+1;i>=1;i--) {
		a[i]=a[i-1];
		b[i]=b[i-1];
		c[i]=c[i-1];
		d[i]=d[i-1];
	}	
}
void Cubic_Spline(int n, double x[], double f[], int Type, double s0, double sn, double a[], double b[], double c[], double d[]) {
	if (Type==1) {
		clamped(n,x,f,s0,sn,a,b,c,d);
	} else {
		natural(n,x,f,s0,sn,a,b,c,d);	
	}
}
double S( double t, double Fmax, int n, double x[], double a[], double b[], double c[], double d[] ) {
	if (t<x[0]||t>x[n]) 
		return Fmax;
	for (int i=0;i<n;i++) 
		if (t>=x[i] && t<=x[i+1]) 
			return a[i+1]+b[i+1]*(t-x[i])+c[i+1]*(t-x[i])*(t-x[i])+d[i+1]*(t-x[i])*(t-x[i])*(t-x[i]);
}
