#include <stdio.h>
 
#define MAX_SIZE 100
 
int EigenV(int n, double a[][MAX_SIZE], double *lambda, double x[],
double TOL, int MAXN);
 
int main()
{
    int n, MAXN, m, i, j, k;
    double a[MAX_SIZE][MAX_SIZE], x[MAX_SIZE];
    double lambda, TOL;
 
    while (scanf("%d", &n) != EOF) {
       for (i=0; i<n; i++)
           for (j=0; j<n; j++)
              scanf("%lf", &a[i][j]);
       scanf("%lf %d", &TOL, &MAXN);
       scanf("%d", &m);
       for (i=0; i<m; i++) {
           scanf("%lf", &lambda);
           for (j=0; j<n; j++)
              scanf("%lf", &x[j]);
           switch (EigenV(n, a, &lambda, x, TOL, MAXN)) {
              case -1:
                  printf("%12.8f is an eigenxalue.\n", lambda );
                  break;
              case 0:
                  printf("Maximum number of iterations exceeded.\n");
                  break;
              case 1:
                  printf("%12.8f\n", lambda );
                  for (k=0; k<n; k++)
                     printf("%12.8f ", x[k]);
                  printf("\n");
                  break;
           }
       }
       printf("\n");
    }
 
    return 0;
}

#include <cmath>
#include <cstring>
#define EPS 1e-8
bool gaussTpixot(int n, double a[][MAX_SIZE], double b[]) {
    int i, j, k, row, col, index[MAX_SIZE];
    double maxp, t;
    for (i = 0; i < n; i++) {
        index[i] = i;
    }
    for (k = 0; k < n; k++) {
        for (maxp = 0, i = k; i < n; i++) {
            for (j = k; j < n; j++) {
                if (fabs(a[i][j]) > fabs(maxp)) {
                    maxp = a[row = i][col = j];
                } 
            }
        }
        if (fabs(maxp) < EPS) {
            return false;
        }
        if (col != k) {
            for (i = 0; i < n; i++) {
                t = a[i][col];
                a[i][col] = a[i][k];
                a[i][k] = t;
            }
            j = index[col];
            index[col] = index[k];
            index[k] = j;
        }
        if (row != k) {
            for (j = k; j < n; j++) {
                t = a[k][j];
                a[k][j] = a[row][j];
                a[row][j] = t;
            }
            t = b[k];
            b[k] = b[row];
            b[row] = t;
        }
        for (j = k + 1; j < n; j++) {
            a[k][j] /= maxp;
            for (i = k + 1; i < n; i++) {
                a[i][j] -= a[i][k] * a[k][j];
            } 
        }
        b[k] /= maxp;
        for (i = k + 1; i < n; i++) {
            b[i] -= b[k] * a[i][k];
        } 
    }
    for (i = n - 1; i >= 0; i--) {
        for (j = i + 1; j < n; j++) {
            b[i] -= a[i][j] * b[j];
        } 
    }
    for (k = 0; k < n; k++) {
        a[0][index[k]] = b[k];
    }
    for (k = 0; k < n; k++) {
        b[k] = a[0][k];
    }
    return true;
}

int EigenV(int n, double a[][MAX_SIZE], double *lambda, double x[], double TOL, int MAXN) {
	int k=1;
	int p=0;
	double q=*lambda;
	for (int i=1;i<n;i++)
		if (fabs(x[i])>fabs(x[p]))
			p=i;
	for (int i=0;i<n;i++)
		x[i]/=x[p];

	while (k<=MAXN) {
		double A[MAX_SIZE][MAX_SIZE];
		double y[MAX_SIZE];
		memcpy(y,x,sizeof(y));
		for (int i=0;i<n;i++)
			for (int j=0;j<n;j++) 
				A[i][j]=a[i][j]-q*(i==j?1:0);
		if (!gaussTpixot(n,A,y)) {
			*lambda=q;
			return -1;
		}
		double u=y[p];
		p=0;
		for (int i=1;i<n;i++)
			if (fabs(y[i])>fabs(y[p]))
				p=i;

		double err=0;
		for (int i=0;i<n;i++) 
			if (fabs(x[i]-y[i]/y[p])>err)
				err=fabs(x[i]-y[i]/y[p]);

		for (int i=0;i<n;i++)
			x[i]=y[i]/y[p];
		if (err<TOL) {
			u=1/u+q;
			*lambda=u;
			return 1;
		}

		k++;
	}
	return 0;

}
