#include <stdio.h>
#include <math.h>

#define MAX_SIZE 100
#define bound pow(2, 127)
#define ZERO 0.000000001 /* X is considered to be 0 if |X|<ZERO */

int Jacobi( int n, double a[][MAX_SIZE], double b[], double x[],
	double TOL, int MAXN );

int Gauss_Seidel( int n, double a[][MAX_SIZE], double b[], double x[],
	double TOL, int MAXN );

int main()
{
	int n, MAXN, i, j, k;
	double a[MAX_SIZE][MAX_SIZE], b[MAX_SIZE], x[MAX_SIZE];
	double TOL;

	while (scanf("%d", &n) != EOF) {
		for (i=0; i<n; i++) {
			for (j=0; j<n; j++)
				scanf("%lf", &a[i][j]);
			scanf("%lf", &b[i]);
		}
		scanf("%lf %d", &TOL, &MAXN);

		printf("Result of Jacobi method:\n");
		for ( i=0; i<n; i++ )
			x[i] = 0.0;
		k = Jacobi( n, a, b, x, TOL, MAXN );
		switch ( k ) {
		case -2:
			printf("No convergence.\n");
			break;
		case -1:
			printf("Matrix has a zero column.  No unique solution exists.\n");
			break;
		case 0:
			printf("Maximum number of iterations exceeded.\n");
			break;
		default:
			printf("no_iteration = %d\n", k);
			for ( j=0; j<n; j++ )
				printf("%.8lf\n", x[j]);
			break;
		}

		printf("Result of Gauss-Seidel method:\n");
		for ( i=0; i<n; i++ )
			x[i] = 0.0;
		k = Gauss_Seidel( n, a, b, x, TOL, MAXN );
		switch ( k ) {
		case -2:
			printf("No convergence.\n");
			break;
		case -1:
			printf("Matrix has a zero column.  No unique solution exists.\n");
			break;
		case 0:
			printf("Maximum number of iterations exceeded.\n");
			break;
		default:
			printf("no_iteration = %d\n", k);
			for ( j=0; j<n; j++ )
				printf("%.8lf\n", x[j]);
			break;
		}
		printf("\n");
	}
	return 0;
}

#include <algorithm>
using namespace std;
int init(int n, double a[][MAX_SIZE],double b[]) {
	for (int i=0;i<n;i++) {
		int maxindex=i;
		for (int j=i+1;j<n;j++) {
			if (fabs(a[j][i])>fabs(a[maxindex][i]))
				maxindex=j;
		}
		if (fabs(a[maxindex][i])>=ZERO) {
			for (int s=0;s<n;s++)
				swap(a[maxindex][s],a[i][s]);
			swap(b[i],b[maxindex]);
		} else {
			for (int j=i-1;j>=0;j--) {
				if (fabs(a[j][i])>fabs(a[maxindex][i]))
					maxindex=j;
			}
			if (fabs(a[maxindex][i])<=ZERO)
				return -1;
			for (int s=0;s<n;s++)
				a[i][s]+=a[maxindex][s];
			b[i]+=b[maxindex];
		}
	}
}
int Jacobi( int n, double a[][MAX_SIZE], double b[], double XO[], double TOL, int MAXN ) {
	if (init(n,a,b)==-1)
		return -1;
	int k=1;
	double x[MAX_SIZE];
	while (k<=MAXN) {
		for (int i=0;i<n;i++) {
			x[i]=0;
			for (int j=0;j<n;j++)
				if (j!=i)
					x[i]-=a[i][j]*XO[j];
			x[i]+=b[i];
			if (fabs(a[i][i])<ZERO)
				return -1;
			x[i]/=a[i][i];
		}
		double max=0;
		for (int i=0;i<n;i++) {
			if (fabs(x[i]-XO[i])>max)
				max=fabs(x[i]-XO[i]);
			if (fabs(x[i])>bound)
				return -2;
		}
		for (int i=0;i<n;i++)
			XO[i]=x[i];		
		if (max<TOL) {
			return k;
		}
		k++;
	}
	return 0;
}
int Gauss_Seidel( int n, double a[][MAX_SIZE], double b[], double XO[], double TOL, int MAXN ) {
	if (init(n,a,b)==-1)
		return -1;
	int k=1;
	while (k<=MAXN) {
		double max=0;		
		for (int i=0;i<n;i++) {
			double org=XO[i];
			XO[i]=0;
			for (int j=0;j<i;j++)
				XO[i]-=a[i][j]*XO[j];
			for (int j=i+1;j<n;j++)
				XO[i]-=a[i][j]*XO[j];
			XO[i]+=b[i];
			if (fabs(a[i][i])<ZERO)
				return -1;
			XO[i]/=a[i][i];
			if (fabs(XO[i]-org)>max)
				max=fabs(XO[i]-org);
			if (fabs(XO[i])>bound)
				return -2;			
		}
		if (max<TOL) {
			return k;
		}
		k++;
	}
	return 0;
}
