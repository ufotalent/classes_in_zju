
#include <stdio.h>
#include <math.h>

#define ZERO 0.000000001 /* X is considered to be 0 if |X|<ZERO */
#define MAXN 11   /* Max Polynomial Degree + 1 */

double Polynomial_Root(int n, double c[], double a, double b, double EPS);

int main()
{
	int n;
	double c[MAXN], a, b;
	double EPS = 0.00005;
	int i;

	while (scanf("%d", &n)!= EOF){
		for (i=n; i>=0; i--)
			scanf("%lf", &c[i]);
		scanf("%lf %lf", &a, &b);
		printf("%.4lf\n", Polynomial_Root(n, c, a, b, EPS));
	}

	return 0;
}
#include <cstring>
#include <cassert>
double C[MAXN];
int N;
double f(double x) {
	double res=0;
	for (int i=N;i>=0;i--)  {
		res=res*x+C[i];
	}
	return res;
}
int sign(double x) {
	if (x>=ZERO)
		return 1;
	if (x<=-ZERO)
		return -1;
	return 0;
}
double binsearch(double a, double b, double EPS) {
	if (b-a<=1e-12) return (a+b)/2;
	double mid=(a+b)/2;
	if (f(a)*f(mid)<=0) 
		return binsearch(a,mid,EPS);
	else
		return binsearch(mid,b,EPS);
}
double Polynomial_Root (int n,double c[], double a, double b, double EPS) {
	N=n;
	memcpy(C,c,sizeof(C));
	/*	static int gao=0;
		static int tot=0;
		tot++;
		gao+=!((f(a)*f(b)<0));
		assert(gao<=5);
		if (tot>20) 
		while (1);*/
	if (a>b) {
		double t=a;
		a=b;
		b=t;
	}
	double res1,res2;
	double min=1e100;
	for (double x=a;x<=b;x+=0.0001) {
		if (fabs(f(x)) < min) {
			min=fabs(f(x));
			res2=x;
		}
	}	
	if (f(a)*f(b)<0) {
		res1=binsearch(a,b,EPS);
	} else {
		res1=res2;
	}

//	if (fabs(res1-res2)>1e-3) {
//		assert(a<1e-3);
//	}
//	assert(fabs(res1-res2) < 1e-5);
	if (fabs(res2)<EPS) {
		res2=0;
	}
	return res2;

}
