#include <stdio.h>

#define Max_size 10000 /* max number of dishes */
void Price( int n, double p[] );
 
int main()
{
    int n, i;
    double p[Max_size];
 
    while (scanf("%d", &n)!=EOF) {
       for (i=0; i<n; i++)
           scanf("%lf", &p[i]);
       Price(n, p);
       for (i=0; i<n; i++)
           printf("%.2f ", p[i]);
       printf("\n");
    }
 
    return 0;
}
#include <cstring>
#include <cassert>
#include <cmath>
double co0[Max_size+100];
double co1[Max_size+100];
double con[Max_size+100];
double res[Max_size+100];
void Price(int n, double p[]) {
	memset(co0,0,sizeof(co0));
	memset(co1,0,sizeof(co1));
	memset(con,0,sizeof(con));
	co0[0]=1;
	co1[1]=1;
	for (int i=1;i<n-1;i++) {
		con[i+1]=p[i]-4*con[i]-con[i-1];
		co0[i+1]=-4*co0[i]-co0[i-1];
		co1[i+1]=-4*co1[i]-co1[i-1];
		if (con[i+1]>1e100) {
			con[i+1]/=1e10;
			co0[i+1]/=1e10;
			co1[i+1]/=1e10;			
			con[i]/=1e10;
			co0[i]/=1e10;
			co1[i]/=1e10;						
		}
	}
	//a1*x0+b1*x1=c1
	//a2*x0+b2*x1=c2
	double a1=co0[n-2]+4*co0[n-1]+1;
	double b1=co1[n-2]+4*co1[n-1];
	double c1=p[n-1]-con[n-2]-4*con[n-1];
	double a2=co0[n-1]+4;
	double b2=co1[n-1]+1;
	double c2=p[0]-con[n-1];
	assert(b2-b1*a2/a1);
	assert(a1);
	res[1]=(c2-c1*a2/a1)/(b2-b1*a2/a1);
	res[0]=(c1-b1*res[1])/a1;
	for (int i=2;i<n;i++) {
		res[i]=co0[i]*res[0]+co1[i]*res[1]+con[i];
		printf("%le %le %le \n",co0[i],co1[i],con[i]);
	}
	printf("solved: %lf %lf\n",res[0],res[1]);
	for (int i=0;i<n;i++) {
		p[i]=res[i]*2;
		if (fabs(p[i])<1e-7)
			p[i]=0;
	}
}
