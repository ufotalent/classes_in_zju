#include <stdio.h>

#define Max_size 10000 /* max number of dishes */
void Price( int n, double p[] );

int main()
{
	int n, i;
	double p[Max_size];

	while (scanf("%d", &n)!=EOF) {
		for (i=0; i<n; i++)
			scanf("%lf", &p[i]);
		Price(n, p);
		for (i=0; i<n; i++)
			printf("%.2f ", p[i]);
		printf("\n");
	}

	return 0;
}
#include <cstring>
#include <cassert>
#include <cmath>
#define D if (0)
double A[10005][4];
double B[10005];
void Price(int n, double p[]) { 
	for (int i=0;i<n;i++)
		B[i]=p[i];
	A[0][0]=2;
	A[0][1]=0.5;
	A[0][2]=0.5;
	A[n-1][0]=0.5;
	A[n-1][1]=0.5;
	A[n-1][2]=2;
	for (int i=1;i<n-2;i++) {
		double rate=0.5/A[i-1][0];
		A[i][0]=2-rate*A[i-1][1];
		A[i][1]=0.5;
		A[i][2]=-A[i-1][2]*rate;
		B[i]-=B[i-1]*rate;

		rate=A[n-1][0]/A[i-1][0];
		A[n-1][0]=-A[i-1][1]*rate;
		A[n-1][2]-=A[i-1][2]*rate;
		B[n-1]-=B[i-1]*rate;
D{		for (int i=0;i<n;i++)
			printf("%lf %lf %lf | %lf\n",A[i][0],A[i][1],A[i][2],B[i]);
		printf("-----------\n");}
	}
	A[n-2][0]=0.5;
	A[n-2][1]=2;
	A[n-2][2]=0.5;
D{	for (int i=0;i<n;i++)
		printf("%lf %lf %lf | %lf\n",A[i][0],A[i][1],A[i][2],B[i]);
	printf("-----------\n");}

	for (int i=n-2;i<n;i++) {
		double rate=A[i][0]/A[n-3][0];
		A[i][1]-=rate*A[n-3][1];
		A[i][2]-=rate*A[n-3][2];		
		A[i][0]=A[i][1];
		A[i][1]=A[i][2];
		A[i][2]=0;
		B[i]-=rate*B[n-3];
	}

	double rate=A[n-1][0]/A[n-2][0];
	A[n-1][0]=A[n-1][1]-A[n-2][1]*rate;
	A[n-1][1]=0;
	B[n-1]-=rate*B[n-2];
D{	for (int i=0;i<n;i++)
		printf("%lf %lf %lf | %lf\n",A[i][0],A[i][1],A[i][2],B[i]);
	printf("-----------\n");}
	rate=A[n-2][1]/A[n-1][0];
	A[n-2][1]=0;
	B[n-2]-=B[n-1]*rate;
	for (int i=n-3;i>=0;i--) {
		double rate=A[i][1]/A[i+1][0];
		A[i][1]=0;
		B[i]-=rate*B[i+1];
		rate=A[i][2]/A[n-1][0];
		A[i][2]=0;
		B[i]-=rate*B[n-1];
	}
D{	for (int i=0;i<n;i++)
		printf("%lf %lf %lf | %lf\n",A[i][0],A[i][1],A[i][2],B[i]);
	printf("-----------\n");	}
	for (int i=0;i<n;i++)
		p[i]=B[i]/A[i][0];
}
