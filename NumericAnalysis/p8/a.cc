
#include <stdio.h>
#include <math.h>
 
double f0( double x, double l, double t )
{
    return sqrt(1.0+l*l*t*t*cos(t*x)*cos(t*x));
}
 
double Integral(double a, double b, double (*f)(double x, double y, double z),double eps, double l, double t);
 
int main()
{
    double a=0.0, b, eps=0.005, l, t;
 
    while (scanf("%lf %lf %lf", &l, &b, &t) != EOF)
       printf("%.2f\n", Integral(a, b, f0, eps, l, t));
    return 0;
}

double _Integral(double a, double b, double (*f)(double x, double y, double z),double eps, double l, double t) {
	double R[5][100];
	double h=b-a;
	R[1][1]=h/2*(f(a,l,t)+f(b,l,t));
	int i=1;
	while (1) {
		i++;
		R[2][1]=R[1][1];
		for (int k=1;k<=1<<(i-2);k++)
			R[2][1]+=h*f(a+(k-0.5)*h,l,t);
		R[2][1]/=2;
		for (int j=2;j<=i;j++)
			R[2][j]=R[2][j-1]+(R[2][j-1]-R[1][j-1])/((1ll<<(2*(j-1)))  -1);
/*		printf("%d\n",i,R[2][i]);
		for (int s=0;s<i;s++)
			printf("%lf ",R[2][s+1]);
		printf("\n%lf\n",fabs(R[2][i]-R[1][i-1]));
*/		if (fabs(R[2][i]-R[1][i-1])<eps)
			return R[2][i]/100;
		h/=2;
		for (int j=1;j<=i;j++)
			R[1][j]=R[2][j];
	}
}

double Integral(double a, double b, double (*f)(double x, double y, double z),double eps, double l, double t) {
	double T=M_PI/2/t;
	int num=b/T;
	if (num) {
        return _Integral(num*T,b,f,eps/num/2,l,t)+num*_Integral(a,T,f,eps/num/2,l,t);
	} else
		return _Integral(a,b,f,eps,l,t);
}
