#include <cstdlib>
#include <stdio.h>

void Series_Sum( double sum[] );

int main()
{
	int i;
	double x, sum[3001];

	Series_Sum( sum );

	x = 0.0;
	for (i=0; i<3001; i++)
		printf("%6.2lf %16.12lf\n", x + (double)i * 0.10, sum[i]);

	return 0;
}
const double eps=1e-15;
double abs(double s) {
	if (s<0)
		return -s;
	else
		return s;
}
double calc(double x) { // calculate sum( 1/(k*(k+1)*(k+2)*(k+x)) )
	double res=0;
	int i;
	for (i=1;abs( (1-x)*(2-x)/3/i/i/i )>eps;i++) {
		res+=1.0/i/(i+1)/(i+2)/(i+x);
	}
	res+=1.0/i/(i+1)/(i+2)/3;
	return res;
}
double f(double x) {
	double res=calc(x);
	res=res*(2-x)+0.25;
	res=res*(1-x)+1;
	return res;
}
void Series_Sum( double sum[] ) {
	for (int i=0;i<=3000;i++) {

		if (i<=10) {
			sum[i]=f(i/10.0);			
		} else {
			double x=i/10.0-1;
			sum[i]=x/(x+1)*sum[i-10]+1/(x+1)/(x+1);
		}

	}
}
