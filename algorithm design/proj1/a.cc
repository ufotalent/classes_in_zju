#include <cstring>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

char *readStr() {
	vector <char> buf;
	char c;
	while ((c=getchar())!='\n') 
		buf.push_back(c);
	char *ret = (char *)malloc(sizeof(char)*(buf.size()+1));
	for (int i=0;i<buf.size();i++) 
		ret[i] = buf[i];
	ret[buf.size()] = 0;
	return ret;
}
char *rev(char *str) {
	char *ret =(char *) malloc(sizeof(char) * (strlen(str)+1));
	strcpy(ret,str);
	reverse(ret,ret+strlen(ret));
	return ret;
}
int* DP[2];
int lcsB(int m, int n, char *A, char *B, int* LL) {
	for (int i=0;i<=n;i++)
		DP[0][i] = 0;
	for (int i=1;i<=m;i++) {
		int k = i&1;
		DP[k][0]=0;
		for (int j=1;j<=n;j++) {
			if (A[i-1] == B[j-1]) 
				DP[k][j] = DP[1-k][j-1]+1;
			else
				DP[k][j] = max(DP[1-k][j],DP[k][j-1]);
		}
	}
	for (int i=0;i<=n;i++)
		LL[i] = DP[m&1][i];
	return DP[m&1][n];
}
 
int *L1,*L2;
string lcs(int m, int n, char *A, char *B,char *revA, char *revB) {
	if (n==0)
		return "";
	if (m==1) {
		for (int i=0;i<n;i++)
			if (B[i]==A[0])
				return string("")+A[0];
		return "";
	}
	int i = m/2;
	lcsB(i,n,A,B,L1);
	lcsB(m-i,n,revA,revB,L2);

	int k = -1;
	int M = -1;
	for (int j=0;j<=n;j++) {
		if (L1[j]+L2[n-j]>M) {
			M = L1[j]+L2[n-j];
			k = j;
		}
	}
	return lcs(i,k,A,B,revA+(m-i), revB+(n-k))+lcs(m-i,n-k,A+i,B+k,revA,revB);
}
int main() {
	char *A = readStr();
	char *B = readStr();
	char *rA = rev(A);
	char *rB = rev(B);
	int m =strlen(A);
	int n = strlen(B);
	char *C = (char *)malloc(sizeof(char) *(m+100) );
	DP[0] =(int *) malloc(sizeof(int) * (n+1));
	DP[1] =(int *) malloc(sizeof(int) * (n+1));
	L1 = (int *) malloc(sizeof(int) * (n+1));
	L2 = (int *) malloc(sizeof(int) * (n+1));
	string res = lcs(m,n,A,B,rA,rB).c_str();
	printf("%d %s\n",res.length(),res.c_str());
	
}
