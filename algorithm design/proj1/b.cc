#include <cstdio>
#include <cstring>
#include <cassert>
#include <algorithm>
using namespace std;
const int MAXM = 300;
const int MAXN = 300;
const int MAXP = 100;
int T[MAXM][MAXN][MAXP];
int TP[MAXM][MAXN][MAXP];
int TPP[MAXM][MAXN][MAXP];
char x[MAXM];
char y[MAXN];
char z[MAXP];
int main() {
	gets(x);
	gets(y);
	gets(z);
	int m = strlen(x);
	int n = strlen(y);
	int p = strlen(z);
	for (int i=1;i<=m;i++)
		for (int j=1;j<=n;j++)
			for (int k=0;k<=p;k++) {
				if ( (k==1 || (k>1 && T[i-1][j-1][k-1]>0 )) && (x[i-1]==y[j-1] && x[i-1]==z[k-1])   ) 
					TP[i][j][k] = 1+T[i-1][j-1][k-1];
				if ((k==0 || T[i-1][j-1][k]>0) && x[i-1]==y[j-1])
					TPP[i][j][k] = 1+T[i-1][j-1][k];
				T[i][j][k] =max(max(max(TP[i][j][k] , TPP[i][j][k]),T[i][j-1][k] ), T[i-1][j][k]);
			}
	printf("%d\n",T[m][n][p]);
}
