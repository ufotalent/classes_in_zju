#! /bin/bash

# usage: view $filename
# output the name of owner and the date of last modification
if [ $# != 1 ]
then
	echo "usage: $0 file"
	exit 1
fi
if [ ! -f $1 ]
then
	echo "usage: $0 file"
	exit 1
fi

set -- $(ls -l $1)

echo "owner: $3"
echo "last date: $6 $7 $8"





