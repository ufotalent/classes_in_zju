#include <stdio.h>
#include <stdlib.h>
int main() {
	mkfifo("fifo",0644);
	while (1) {
		static int seq=0;
		seq++;
		FILE *fp = fopen("fifo","w");
		char buf[200];
		sprintf(buf,"Signal %d",seq);
		fprintf(fp,"%s\n",buf);
		fclose(fp);
		printf("Signal %d sent\n",seq);
		sleep(1);
	}
}
