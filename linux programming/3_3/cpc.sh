#! /bin/bash

# usage: cpc.sh $dir
# create a new directory named $dir and copy all .c files in the current directory into $dir

if [ $# != 1 ]
then
	echo "usage: $0 dir"
	exit 1
fi

dir=$1
while [ -d $dir ]
do
	echo "directory exists!"
	read dir
done

mkdir $dir

set -- $(ls -a)

for filename in $*
do
	if [ -f $filename ]
	then
		len=$(expr length $filename)
		lasttwo=$(expr substr $filename $(( $len - 1 )) 2)
		if [ $lasttwo = ".c" ] 
		then
			echo "copying file $filename"
			cp $filename $dir
		fi
	fi
done


