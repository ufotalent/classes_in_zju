#! /bin/bash

# usage: sync $src $dest
# make a newest backup in $dest for $src
if [ $# != 2 ]
then
	echo "usage: $0 dir1 dir2"
	exit 1
fi

if [ -d $1 -a -d $2 ] 
then
	from=$1
	to=$2
	filelist=$(ls -A $from)
	for name in $filelist
	do
# call sync to handle directories
		if [ -d "$from/$name" ]
		then
			if [ ! -d "$to/$name" ]
			then
				mkdir $to/$name
			fi
			bash $0 $from/$name $to/$name
		fi
		res=$(find $to -newer $from/$name -name $name);
		res=${res:+"ex"}
		if [ "$res" != "ex" ]
		then
			echo sync $name
			cp $from/$name $to -r

		fi

	done




else
	echo "usage: $0 dir1 dir2"
	exit 1

fi

