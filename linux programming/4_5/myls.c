#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>


#include "ls.h"
//#define DEBUG
struct jobnode {
	struct jobnode *next;
	char *filename;
};
typedef struct jobnode *pjobnode;


void extractCommand(char *cmd, bool res[]) {
	char *c;
	for (c=cmd; *c; c++) {
		res[*c]=true;
	}
}

void extractFilename(char *cmd, pjobnode *head, pjobnode *tail) {
	if (!*cmd)
		return;
	if (*head == NULL) {
		assert(*tail == NULL);
		*head = *tail = (pjobnode) malloc(sizeof(struct jobnode));
		(*head) -> next=NULL;
	} else {
		(*tail) -> next = (pjobnode) malloc(sizeof(struct jobnode));
		(*tail) -> next -> next=NULL;
		(*tail) = (*tail) -> next;
	}
	(*tail) -> filename = (char *)malloc(strlen(cmd)+1);
	strcpy((*tail) -> filename,cmd);
}
int main(int argc,char *argv[]) {
	bool flags[1<<8];	

	pjobnode head=NULL,tail=NULL;
	memset(flags,0,sizeof(flags));
	int i;
	for (i=1;i<argc;i++) {
		if (argv[i][0]=='-') 
			extractCommand(argv[i]+1,flags);
		else 
			extractFilename(argv[i],&head,&tail);
	}
	if (head == NULL) {
		char cwd[200];
		getcwd(cwd,sizeof(cwd));
		extractFilename(cwd,&head,&tail);
	}
#ifdef DEBUG
	pjobnode p;
	char c;
	for (p=head;p;p=p->next) 
		printf("%s\n",p->filename);
	for (c=0;c<127;c++)
		if (flags[c])
			putchar(c);

#endif
	pjobnode it;
	for (it=head;it;it=it->next) {
		bool print = (head != tail) || flags['R'];
		if (!ls(it->filename,flags,print)) 
			printf("can not access %s\n",it->filename);
	}
	printf("\n");
}
