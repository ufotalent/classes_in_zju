#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <fcntl.h>
#include <dirent.h>
#include <time.h>
#include <assert.h>
#include "ls.h"


struct node {
	char name[200];
	time_t mtime;
};
typedef struct node *pnode;
void prusr(int uid,int len) {
	struct passwd *now=getpwuid(uid);
	int nowlen=strlen(now->pw_name);
	printf("%s",now->pw_name);
	int i;
	for (i=nowlen;i<len;i++)
		printf(" ");
	printf(" ");
}

void prgrp(int gid,int len) {
	struct group *now=getgrgid(gid);
	int nowlen=strlen(now->gr_name);
	printf("%s",now->gr_name);
	int i;
	for (i=nowlen;i<len;i++)
		printf(" ");
	printf(" ");	
}
void prprv(int mode) {
	if (S_ISBLK(mode))
		printf("b");
	if (S_ISCHR(mode))
		printf("c");
	if (S_ISDIR(mode))
		printf("d");
	if (S_ISFIFO(mode))
		printf("p");
	if (S_ISREG(mode))
		printf("-");
	if (S_ISLNK(mode))
		printf("l");
	if (S_IRUSR&mode)
		printf("r");
	else
		printf("-");
	if (S_IWUSR&mode)
		printf("w");
	else
		printf("-");
	if (S_IXUSR&mode)
		printf("x");
	else
		printf("-");	
	if (S_IRGRP&mode)
		printf("r");
	else
		printf("-");
	if (S_IWGRP&mode)
		printf("w");
	else
		printf("-");
	if (S_IXGRP&mode)
		printf("x");
	else
		printf("-");	
	if (S_IROTH&mode)
		printf("r");
	else
		printf("-");
	if (S_IWOTH&mode)
		printf("w");
	else
		printf("-");
	if (S_IXOTH&mode)
		printf("x");
	else
		printf("-");		
	printf(" ");
}
void prftr(int mode) {
	if (S_ISDIR(mode))
		printf("/");
	else if ((S_IXOTH&mode) && (S_IXGRP&mode) && (S_IXUSR&mode))
		printf("*");
	else if (S_ISFIFO(mode))
		printf("|");
	else if (S_ISLNK(mode))
		printf("@");
    else if (S_ISSOCK(mode))
		printf("=");

}
bool firstline=true;
bool lsfile(char *name, bool inode, bool list,bool fitter,int ulen,int glen,int lnlen,int szlen,int inlen) {

	struct stat now;
	if (lstat(name,&now)==EOF)
		return false;
	firstline=false;	
	if (inode) {
		char buf[10];
		sprintf(buf,"%%%dd ",inlen);
		printf(buf,now.st_ino,name);	
	}
	if (list) {
		prprv(now.st_mode);
		char buf[10];
		sprintf(buf,"%%%dd ",lnlen);
		printf(buf,now.st_nlink);
		prusr(now.st_uid,ulen);
		prgrp(now.st_gid,glen);
		sprintf(buf,"%%%dd ",szlen);
		printf(buf,now.st_size);		
		time_t t=now.st_mtime;
		char *c=ctime(&t);
		*(c+strlen(c)-9)=0;
		printf("%s ",c+4);
	} 
	printf("%s",name);
	if (fitter) 
		prftr(now.st_mode);
	if (list)
		printf("\n");
	else
		printf("  ");
	return true;
}
int ncount(int n) {
	int res=0;
	while (n) {
		n/=10;
		res++;
	}
	return res;
}
struct node files[1<<16];
bool sort_by_mtime;
int cmp(pnode a,pnode b) {
	if (sort_by_mtime) 
		return a->mtime< b->mtime;
	else
		return strcmp(a->name,b->name);
}
bool lsdir(char *name, bool recur,bool inode,bool list,bool fitter,bool sbtime,bool all,bool print) {
	char orgdir[200];
	getcwd(orgdir,sizeof(orgdir));
	DIR *dp;
	struct dirent *ret;
	int i;
	if ((dp=opendir(name))==NULL)
		return false;

	if (firstline) 
		firstline=false;
	else
		printf("\n\n");
	int ulen=0;
	int glen=0;
	int lnlen=0;
	int szlen=0;
	int inlen=0;
	int blocks=0;
	int num=0;
	chdir(name);
	assert(dp);
	while (ret=readdir(dp)) {
		struct stat st;
		lstat(ret->d_name,&st);
		if (ret->d_name[0]=='.' && !all)
			continue;		
		blocks+=st.st_blocks;
		files[num].mtime=st.st_mtime;
		strcpy(files[num].name,ret->d_name);
		num++;
		struct passwd *now=getpwuid(st.st_uid);
		if (strlen(now->pw_name)> ulen) {
			ulen=strlen(now->pw_name);
		}
		struct group *grp=getgrgid(st.st_gid);
		if (strlen(grp->gr_name)> glen) {
			glen=strlen(grp->gr_name);
		}		
		if (ncount(st.st_nlink)>lnlen)
			lnlen=ncount(st.st_nlink);
		if (ncount(st.st_size)>szlen)
			szlen=ncount(st.st_size);
		if (ncount(st.st_ino)>inlen)
			inlen=ncount(st.st_ino);
	}
	closedir(dp);
	chdir(orgdir);

	sort_by_mtime=sbtime;
	qsort(files,num,sizeof(struct node),cmp);
	chdir(name);	
	char cdir[200];
	getcwd(cdir,sizeof(cdir));
	if (print)
		printf("%s:\n",cdir);
	if (list)
		printf("total %d\n",blocks/2);
	for (i=0;i<num;i++) {
		if (files[i].name[0]=='.' && !all)
			continue;
		lsfile(files[i].name,inode,list,fitter,ulen,glen,lnlen,szlen,inlen);	
	}
/*	while (ret=readdir(dp)) {
		if (ret->d_name[0]=='.' && !all)
			continue;
		lsfile(ret->d_name,inode,list,ulen,glen,lnlen,szlen);
	} */
	chdir(orgdir);


	if (recur) {
		char buf[200];
		getcwd(buf,sizeof(buf));
		dp=opendir(name);
		chdir(name);
		assert(dp);
		while (ret=readdir(dp)) {
			struct stat now;
			lstat(ret->d_name,&now);

			if (ret->d_name[0]=='.' && !all)
				continue;
			if (strcmp(ret->d_name,"..") && strcmp(ret->d_name,".") && S_ISDIR(now.st_mode)) {
				lsdir(ret->d_name,recur,inode,list,fitter,sbtime,all,print);
			}
		}
		closedir(dp);
	}
	chdir(orgdir);
	return true;
}

bool ls(char *name,bool res[],bool print) {
	struct stat now;
	lstat(name,&now);
	if (S_ISDIR(now.st_mode) && !res['d']) 
		return lsdir(name,res['R'],res['i'],res['l'],res['F'],res['t'],res['a'],print);
	else
		return lsfile(name,res['i'],res['l'],res['F'],0,0,0,0,0);
}
