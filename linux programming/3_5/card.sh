#! /bin/bash

# a simple card game.
# usage : card.sh



# funtion generate
# generate a new random sequence of 52 cards
# the results are put in array card and present
# array card contains random shuffled numbers from 1 to 52, where card with number 1..13 represent spades, 14..26 represents hearts, etc.
# cards in this array with index 1 to 13 belongs to the first player, 14 to 24 to the second, etc.
# array present contains printable strings to represent those card rather than numbers
# array used contains 52 masks to indicate whether a card has been used
generate() 
{
	for (( i=1; i<=52;i++ ))
	do
		card[$i]=$i;
		used[$i]=false;
		if [ $i -le 13 ]
		then
			present[$i]="A$((  ($i-1) % 13 + 1 ))"
		elif [ $i -le 26 ]
		then
			present[$i]="B$((  ($i-1) % 13 + 1 ))"
		elif [ $i -le 39 ]
		then
			present[$i]="C$((  ($i-1) % 13 + 1 ))"			
		else
			present[$i]="D$((  ($i-1) % 13 + 1 ))"			
		fi
	done
	for (( i=1; i<52;i++ ))
	do
		rand=$(expr $RANDOM % $((53 - i)));
		rand=$(($rand + $i))
		tmp=${card[$i]}
		card[$i]=${card[$rand]}
		card[$rand]=$tmp
		tmp=${present[$i]}
		present[$i]=${present[$rand]}
		present[$rand]=$tmp
	done
}

generate


for ((i=0;i<4;i++))
do
	score[$i]=0
done

for ((round=1;round<=13;round++))
do
	echo "---------------------------------------------"
	echo "Round $round start"
	for ((i=0;i<4;i++))
	do
		echo "player $i has score: ${score[$i]}"
	done
	max=0
	for ((i=0;i<4;i++))
	do
		echo "player $i's turn"
		echo "you have cards:"
		lb=$(($i * 13 + 1))
		ub=$((($i + 1) * 13 + 1))
		for ((j=lb;j<ub;j++  ))
		do
			if [ ${used[$j]} = false ]
			then
				echo -n "${present[j]} "
			fi
		done

		can=false
		while [ $can = false ]
		do
			echo ""
			echo -n "you want to give out :"
			read num
			for ((j=lb;j<ub;j++  ))
			do
				if [ ${used[$j]} = false -a ${present[$j]} = "$num" ]
				then
					used[$j]=true;
					out[$i]=$((  (${card[$j]}-1) % 13 + 1 ))
					can=true;
					break;
				fi
			done
		done

		if [ ${out[$i]} -gt $max ]
		then
			max=${out[$i]}
		fi
	done
	for ((i=0;i<4;i++))
	do

		if [ ${out[$i]} = $max ]
		then
			echo "player $i win 1 score"
			score[$i]=$((${score[$i]} + 1))
		fi
	done
done

echo "---------------------------------------------"
echo "Final score"
for ((i=0;i<4;i++))
do
	echo "player $i has score: ${score[$i]}"
done

