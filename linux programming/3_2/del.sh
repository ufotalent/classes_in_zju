#! /bin/bash

# usage : del.sh $dir
# removes the directory named $dir
#
# alternative usage: del.sh $file $dir
# removes $file if $file is in $dir

if [ $# == 0 ]
then
	echo "usage: $0 file dir or $0 dir"
	exit 1
fi

if [ -d $1 ]
then
	echo "removing $1"
	rm $1 -r
	exit 0
fi
todel=$1
dir=$2
if [ ! -d $dir ]
then
	echo "usage: $0 file dir or $0 dir"
	exit 1
fi

cd $dir

if [ ! -f $todel ]
then
	echo "usage: $0 file dir or $0 dir"
	exit 1
fi

set -- $(ls -a )

delete=false
for name in $*
do
	if [ $name = $todel ]
	then
		rm $name
		delete=true
		break
	fi
done

if [ $delete = true ]
then
	echo "deleted"
else
	echo "not found"
fi

