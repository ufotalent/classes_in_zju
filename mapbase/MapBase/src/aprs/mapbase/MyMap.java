package aprs.mapbase;

import java.util.ArrayList;
import java.util.List;

import com.trolltech.qt.core.Qt.MouseButton;
import com.trolltech.qt.core.Qt.Orientation;
import com.trolltech.qt.gui.QApplication;
import com.trolltech.qt.gui.QBrush;
import com.trolltech.qt.gui.QColor;
import com.trolltech.qt.gui.QDragMoveEvent;
import com.trolltech.qt.gui.QGraphicsItemInterface;
import com.trolltech.qt.gui.QGraphicsScene;
import com.trolltech.qt.gui.QGraphicsTextItem;
import com.trolltech.qt.gui.QGraphicsView;
import com.trolltech.qt.gui.QLineF;
import com.trolltech.qt.gui.QMouseEvent;
import com.trolltech.qt.gui.QPaintEvent;
import com.trolltech.qt.gui.QPen;
import com.trolltech.qt.gui.QResizeEvent;
import com.trolltech.qt.gui.QWheelEvent;
import com.trolltech.qt.gui.QWidget;

public class MyMap extends QGraphicsView{
	
	//MyNode centre=new MyNode(-1,125,125,null);
    private Data data; 
	MyNode testNode;	// for test
	
	static private double _scale=0.00001;	// 		latitude per pixel
	private double _offsetLati=0;
	private double _offsetLong=0;
	private double _centerLati=-30.268;
	private double _centerLong=120.128;
	private boolean _pressed=false;
	private MyWay _track=new MyWay(0, new ArrayList<MyNode>(),"track",MyWay.WAY_TYPE_TRACK);
	private ArrayList<MyNode> _marknodes=new ArrayList<MyNode>();
	static private QGraphicsScene gs=new QGraphicsScene();
	
	static public double getScale() {
		return 1/_scale;
	}
	public MyMap(QWidget parent){
		super(gs,parent);
		try {
			data=new Data();
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		setDragMode(DragMode.ScrollHandDrag);
		//gs.setBackgroundBrush(new QBrush(new QColor(255,255,255)));
		//testNode=new MyNode(1,30,120,new ArrayList<String>());	//for test
		//testNode.setType(MyNode.NODE_TYPE_POLICE);
	}
	
	/**í??ˉμ?í?*/
	
	@Override
	protected void mousePressEvent(QMouseEvent arg0) {
		if (arg0.button()==MouseButton.LeftButton) {
			_offsetLong=_centerLong+arg0.x()*_scale;
			_offsetLati=_centerLati+arg0.y()*_scale;
			_pressed=true;
		}
		if (arg0.button()==MouseButton.RightButton) {
			double Long = (arg0.x()-this.width()/2)*_scale+_centerLong;
			double Lati = (arg0.y()-this.height()/2)*_scale+_centerLati;
			_addTrackNode(Long,Lati);
		}
		
		// TODO Auto-generated method stub
		super.mousePressEvent(arg0);
	}
	
	@Override
	protected void mouseReleaseEvent(QMouseEvent arg0) {
		if (arg0.button()==MouseButton.LeftButton) 
			_pressed=false;
		// TODO Auto-generated method stub
		super.mouseReleaseEvent(arg0);
	}
	@Override
	protected void mouseMoveEvent(QMouseEvent arg0) {
		if (_pressed) {
			_centerLong=_offsetLong-arg0.x()*_scale;
			_centerLati=_offsetLati-arg0.y()*_scale;
			double Long = (arg0.x()-this.width()/2)*_scale+_centerLong;
			double Lati = (arg0.y()-this.height()/2)*_scale+_centerLati;
//			System.err.println("Center: "+_centerLong+"."+_centerLati);
//			System.err.println("Mouse: "+Long+"."+Lati);
//			System.err.println("Abs: "+arg0.x()+"."+arg0.y());
			// TODO Auto-generated method stub
			super.mouseMoveEvent(arg0);
			_redraw();
		}
	}

	

	private void _drawWay(MyWay way) {
		if (way.get_nodes().size()==0)
			return ;
		double Long =way.get_nodes().get(0).getLongitude();
		double Lati =way.get_nodes().get(0).getLatitude();
		double x=(Long-_centerLong)/_scale+this.width()/2-1;
		double y=(Lati-_centerLati)/_scale+this.height()/2-1;
		way.setPos(x, y);
		
		way.setScale(1/_scale);
		gs.addItem(way);
		if (!way.getName().equals("")) {
			for (int i=0;i<way.get_nodes().size();i+=10) {
				MyNode now =way.get_nodes().get(i);
				
				double nowx=(now.getLongitude()-_centerLong)/_scale+this.width()/2-1;
				double nowy=(now.getLatitude()-_centerLati)/_scale+this.height()/2-1;
				QGraphicsTextItem ret= gs.addText(way.getName());
				ret.setPos(nowx, nowy);
				ret.setDefaultTextColor(QColor.red);
						
				
			}
		}
	}
	private void _drawNode(MyNode node) { 
		double Long = node.getLongitude();
		double Lati = node.getLatitude();
		double x=(Long-_centerLong)/_scale+this.width()/2-1;
		double y=(Lati-_centerLati)/_scale+this.height()/2-1;
		node.setPos(x, y);
		node.setScale(1);
		gs.addItem(node);
	}

	private void _redraw() {
//		System.err.println("redraw called");
		double Longmin = (-this.width()/2)*_scale+_centerLong;
		double Latimin = (-this.height()/2)*_scale+_centerLati;
		double Longmax = (this.width()/2)*_scale+_centerLong;
		double Latimax = (this.height()/2)*_scale+_centerLati;

		List <QGraphicsItemInterface> items=gs.items();
		for (int i=0;i<items.size();i++)
			gs.removeItem(items.get(i));

		gs.setSceneRect(0,0,this.width()-2,this.height()-2);
		
//		System.err.println("Asking for "+Longmin+":"+Latimin+":"+Longmax+":"+Latimax+"with scale "+_scale);
		ArrayList<MyNode> nodes=data.findNode(Longmax, Longmin, Latimax, Latimin);
//		System.err.println("found "+nodes.size());
		
		int gap=(int) (Math.sqrt(nodes.size()/200)+1);
		if (gap<4) gap=4;
		boolean used[][]=new boolean[width()/gap][height()/gap];
		for (int i=0;i<width()/gap;i++)
			for (int j=0;j<height()/gap;j++)
				used[i][j]=false;
		
		for (int i=0;i<nodes.size();i++) {
			double x=(nodes.get(i).getLongitude()-_centerLong)/_scale+this.width()/2-1;
			double y=(nodes.get(i).getLatitude()-_centerLati)/_scale+this.height()/2-1;
			int dx=(int) (x/20);
			int dy=(int) (y/20);
			if (dx<0) dx=0;
			if (dy<0) dy=0;
			if (dx>=width()/gap) dx=width()/gap-1;
			if (dy>=height()/gap) dy=height()/gap-1;
			if (!used[dx][dy]||!nodes.get(i).getName().equals("")) {
				_drawNode(nodes.get(i));
				used[dx][dy]=true;
			}
			
		}
		for (int i=0;i<_marknodes.size();i++) {
			_drawNode(_marknodes.get(i));
		}
		
		ArrayList<MyWay> ways=data.findWay(Longmax, Longmin, Latimax, Latimin);
//		System.err.println("find "+ways.size()+" Ways");
		for (int i=0;i<ways.size() && i<50;i++) {
//			System.err.println(ways.get(i).get_nodes().size());
			_drawWay(ways.get(i));
		}		
		_drawWay(_track);
		
		
		gs.update();		
	}
	@Override
	protected void resizeEvent(QResizeEvent arg0) {
		
		/*
		ArrayList<MyWay> ways=data.findWay(Longmax, Longmin, Latimax, Latimin);
		for (int i=0;i<ways.size();i++) {
			System.err.println("Drawing");
			_drawWay(ways.get(i));
		}		
		ArrayList<MyNode> nodes=data.findNode(Longmax, Longmin, Latimax, Latimin);
		System.err.println("found "+nodes.size());
		for (int i=0;i<nodes.size();i++) {
			_drawNode(nodes.get(i));
			//gs.addItem(nodes.get(i));
			
		}*/
		// TODO Auto-generated method stub
		super.resizeEvent(arg0);
		_redraw();
	}

	
	@Override
	protected void wheelEvent(QWheelEvent arg0) {
		if (arg0.orientation()==Orientation.Vertical) {
			if (arg0.delta()>0) {
				_scale=_scale/1.1;
				if (_scale<0.00000006)
					_scale*=1.1;
			} else {
				_scale=_scale*1.1;
			}
		}
//		System.err.println("Scale: "+_scale);
		_redraw();
		// TODO Auto-generated method stub
//		super.wheelEvent(arg0);
//		double numDegrees = -arg0.delta()/ 8.0;
//	    double numSteps = numDegrees / 15.0;
//	    double factor = Math.pow(1.125, numSteps);
//	    scale(factor, factor);//
	}
	
	
	public void clearTracks(){
		_track.clearAllNodesAndReinit();
		_redraw();
		// remind dd that way can be empty
	}
	
	
	private void _addTrackNode(double Longitude,double Latitude) { 
		_track.addNode(new MyNode(-1,Longitude,Latitude,"track",-1));
		_redraw();
	}
	private void _moveCenter(double Longitude,double Latitude){
		_centerLong=Longitude;
		_centerLati=Latitude;
		_redraw();
	}
	
	
	public void addTrackNode(double Longitude,double Latitude) {
		Latitude=-Latitude;
		_track.addNode(new MyNode(-1,Longitude,Latitude,"track",-1));
		_redraw();
	}

	public void moveCenter(double Longitude,double Latitude){
		Latitude=-Latitude;
		_centerLong=Longitude;
		_centerLati=Latitude;
		_redraw();
	}
	public void locate(String text) {
		_marknodes.clear();
		ArrayList<MyNode> nodes=data.markNode(text);
		for (int i=0;i<nodes.size();i++) {
			
			MyNode now=nodes.get(i);
			_marknodes.add(new MyNode(-1,now.getLongitude(),now.getLatitude(),"",MyNode.NODE_TYPE_MARK));
			_moveCenter(now.getLongitude(),now.getLatitude());
		}
		nodes=data.markWay(text);
		for (int i=0;i<nodes.size();i++) {
			
			MyNode now=nodes.get(i);
			_marknodes.add(new MyNode(-1,now.getLongitude(),now.getLatitude(),"",MyNode.NODE_TYPE_MARK));
			_moveCenter(now.getLongitude(),now.getLatitude());
		}
		_redraw();
	}
	
	public void clearLocator(){
		_marknodes.clear();
		_redraw();
	}
	
}

 