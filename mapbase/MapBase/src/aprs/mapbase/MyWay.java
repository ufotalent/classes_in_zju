package aprs.mapbase;

import java.util.ArrayList;

import com.trolltech.qt.core.QPointF;
import com.trolltech.qt.core.QRectF;
import com.trolltech.qt.core.Qt;
import com.trolltech.qt.core.Global.QtMsgType;
import com.trolltech.qt.gui.QColor;
import com.trolltech.qt.gui.QGraphicsItem;
import com.trolltech.qt.gui.QGraphicsScene;
import com.trolltech.qt.gui.QGraphicsView;
import com.trolltech.qt.gui.QPainter;
import com.trolltech.qt.gui.QPainterPath;
import com.trolltech.qt.gui.QPen;
import com.trolltech.qt.gui.QStyleOptionGraphicsItem;
import com.trolltech.qt.gui.QWidget;

public class MyWay extends QGraphicsItem {
	final static public float INNER_SCALE_FACTOR = 100;
	final String[] typeNameArray = { "", "cycleway", "bridge", "railway",
			"waterway", "foot", "oneway", "bicycle" };
	final static double UNIT_WIDTH = 3;
	/** 小路 */
	final static int WAY_TYPE_ROAD = 0;
	QPen pen = new QPen(QColor.green, UNIT_WIDTH, Qt.PenStyle.SolidLine,
			Qt.PenCapStyle.RoundCap, Qt.PenJoinStyle.RoundJoin);
	/** 普通公路 */
	final static int WAY_TYPE_CYCLEWAY = 1;
	/** 高速公路 */
	final static int WAY_TYPE_BRIDGE = 2;
	/** 桥面路段 */
	final static int WAY_TYPE_RAILWAY = 3;
	final static int WAY_TYPE_WATERWAY = 4;
	final static int WAY_TYPE_FOOT = 5;
	final static int WAY_TYPE_ONEWAY = 6;
	final static int WAY_TYPE_BICYCLE = 7;
	final static int WAY_TYPE_TRACK = 8;
	/** 路段ID号，默认为-1 */
	private int _wayID = -1;

	/** 路段上的点集 */
	private ArrayList<MyNode> _nodes = new ArrayList<MyNode>();
	public double wayLength;
	
	public ArrayList<MyNode> get_nodes() {
		return _nodes;
	}

	/** 路的名字,默认为空字符串 */
	private String _roadName = "";

	/** 关于路段的标签arraylist */
	private ArrayList<String> _tags = null;

	/** 路的类型:公路、高速公路、桥 */
	private int _type = WAY_TYPE_ROAD;

	private QRectF _boundingRect = new QRectF();
	private double _leftx, _upy, _downy, _rightx;
	private double _startx = 0, _starty = 0;

	private double _originWidth = UNIT_WIDTH;
	public MyWay(int id, ArrayList<MyNode> nodes, String name, int type) {
		this._wayID = id;
		for (MyNode node : nodes) {
			addNode(node);
		}
		_roadName = name;
		switch (type) {
		case WAY_TYPE_ROAD:
			pen = new QPen(QColor.darkYellow, 1 * UNIT_WIDTH,
					Qt.PenStyle.SolidLine, Qt.PenCapStyle.RoundCap,
					Qt.PenJoinStyle.RoundJoin);
			break;
		case WAY_TYPE_WATERWAY:
			pen = new QPen(QColor.blue, 1 * UNIT_WIDTH,
					Qt.PenStyle.SolidLine, Qt.PenCapStyle.RoundCap,
					Qt.PenJoinStyle.RoundJoin);
			break;
		case WAY_TYPE_CYCLEWAY:
			pen = new QPen(QColor.darkMagenta, 3 * UNIT_WIDTH,
					Qt.PenStyle.SolidLine, Qt.PenCapStyle.RoundCap,
					Qt.PenJoinStyle.RoundJoin);
			break;
		case WAY_TYPE_BRIDGE:
			pen = new QPen(QColor.darkCyan, 3 * UNIT_WIDTH, Qt.PenStyle.DashLine,
					Qt.PenCapStyle.RoundCap, Qt.PenJoinStyle.RoundJoin);
			break;
		case WAY_TYPE_RAILWAY:
			pen = new QPen(QColor.darkRed, 3 * UNIT_WIDTH,
					Qt.PenStyle.SolidLine, Qt.PenCapStyle.RoundCap,
					Qt.PenJoinStyle.RoundJoin);
			break;
		case WAY_TYPE_BICYCLE:
		case WAY_TYPE_ONEWAY:
		case WAY_TYPE_FOOT:
			pen = new QPen(QColor.yellow, 2 * UNIT_WIDTH,
					Qt.PenStyle.SolidLine, Qt.PenCapStyle.RoundCap,
					Qt.PenJoinStyle.RoundJoin);
			break;
		case WAY_TYPE_TRACK:
			pen = new QPen(QColor.black, 0.5 * UNIT_WIDTH,
					Qt.PenStyle.DashDotDotLine, Qt.PenCapStyle.RoundCap,
					Qt.PenJoinStyle.RoundJoin);
		default:
			break;
		}
		_originWidth = pen.widthF();
		// initNameAndType();
	}

	/*
	 * private void initNameAndType(){
	 * roadName=tags.get(Converter.getItem("name")); for(int
	 * j=1;j<Max_Type;++j){ if(tags.get(Converter.getItem(typeNameArray[j])) !=
	 * null){ type=j; return; } } type=WAY_TYPE_ORDINARY; }
	 */

	public void Rename(String name) {
		_roadName = name;
	}

	public String getName() {
		return _roadName;
	}

	private static final double OFFSET = 5;
	private QPainterPath _path = new QPainterPath();
	private QPainterPath _textPath = new QPainterPath();

	public boolean addNode(MyNode n) {
		/**
		 * refresh bounding rectangle
		 */
		double incomingx = n.getLongitude();// x
		double incomingy = n.getLatitude();// y
		if (incomingx < _leftx)
			_leftx = incomingx;
		else if (incomingx > _rightx)
			_rightx = incomingx;
		if (incomingy < _downy)
			_downy = incomingy;
		else if (incomingy > _upy)
			_upy = incomingy;
		_boundingRect.setBottom(_downy - OFFSET);
		_boundingRect.setTop(_upy + OFFSET);
		_boundingRect.setLeft(_leftx - OFFSET);
		_boundingRect.setRight(_rightx + OFFSET);
		/**
		 * setting draw path
		 */
		if (_nodes.size() == 0) {
			_startx = n.getLongitude();
			_starty = n.getLatitude();
		} else {
			_path.lineTo(incomingx - _startx, incomingy - _starty);
			_textPath.lineTo(incomingx - _startx, incomingy - _starty);
			// _textPath.addText(new QPointF((incomingx - _startx) / 2,
			// (incomingy - _starty) / 2), new QFont(),"ttt");
		}
		_nodes.add(n);
		return true;
	}

	public void clearAllNodesAndReinit() {
		_nodes.clear();
		_path = new QPainterPath();
		_boundingRect = new QRectF();
		_startx = 0;
		_starty = 0;
	}

	@Override
	public QPainterPath shape() {
		return _path;
	}

	@Override
	public QRectF boundingRect() {
		return _boundingRect;
	}

	QColor color = new QColor(255, 255, 0);
	QPen _textPen = new QPen();

	@Override
	public void paint(QPainter arg0, QStyleOptionGraphicsItem arg1, QWidget arg2) {
		
//		if(MyMap.getScale() * pen.widthF() < 0.1)
//		{
//			System.out.println(MyMap.getScale() * pen.widthF());
//			arg0.drawLine((int)_startx, (int)_starty, (int)_nodes.get(_nodes.size()-1).getLongitude(), (int)_nodes.get(_nodes.size()-1).getLatitude());
//			return;
//		}
		pen.setWidthF(_originWidth/MyMap.getScale());
		arg0.setPen(pen);
//		System.out.println("pen width changed to "+_originWidth/MyMap.getScale());
//		System.out.println("now width" + _originWidth/MyMap.getScale()*MyMap.getScale());
		arg0.save();
		arg0.drawPath(_path);
//		if (needDrawText(arg1)) {
//			arg0.setPen(_textPen);
//			paintText(arg0);
//		}
		arg0.restore();
	}

	private boolean needDrawText(QStyleOptionGraphicsItem arg1) {
		//double scaleFactor = MyMap.
		//System.out.println(scaleFactor);
//		if(scaleFactor * pen.widthF() > 2)
//			return true;
//		else 
//			return false;
		return true;
	}

	QPointF _textPointF = new QPointF();

	private void paintText(QPainter arg0) {
		if (_nodes.size() < 1)
			return;
		double theta = 0;

		for (int i = 1; i < _nodes.size(); i++) {
			double tanValue = ((_nodes.get(i).getLatitude() - _nodes.get(i - 1)
					.getLatitude()) / (_nodes.get(i).getLongitude() - _nodes
					.get(i - 1).getLongitude()));
			theta = Math.atan(tanValue) * 360 / Math.PI;
			arg0.save();
			_textPointF.setX((_nodes.get(i).getLongitude()
					+ _nodes.get(i - 1).getLongitude() - 2 * _startx) / 2);
			_textPointF.setY((_nodes.get(i).getLatitude()
					+ _nodes.get(i - 1).getLatitude() - 2 * _starty) / 2);
			arg0
					.translate(
							(_nodes.get(i).getLongitude()
									+ _nodes.get(i - 1).getLongitude() - 2 * _startx) / 2,
							(_nodes.get(i).getLatitude()
									+ _nodes.get(i - 1).getLatitude() - 2 * _starty) / 2);
			arg0.rotate(90 - theta);
			arg0.drawText(0, 0, _roadName);
			arg0.restore();
		}
	}

	public int getWayID() {
		return _wayID;
	}

	public ArrayList<String> getWayTags() {
		return _tags;
	}

	public int getWayType() {
		return _type;
	}
}
