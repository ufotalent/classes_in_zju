package aprs.mapbase;

import java.util.ArrayList;

import com.trolltech.qt.core.QPointF;
import com.trolltech.qt.core.QRectF;
import com.trolltech.qt.core.Qt;
import com.trolltech.qt.gui.QColor;
import com.trolltech.qt.gui.QGraphicsItem;
import com.trolltech.qt.gui.QPainter;
import com.trolltech.qt.gui.QPen;
import com.trolltech.qt.gui.QPixmap;
import com.trolltech.qt.gui.QStyleOptionGraphicsItem;
import com.trolltech.qt.gui.QWidget;

public class MyNode extends QGraphicsItem{
	final int Max_Type=10;
	final String[] typeNameArray={"","hospital","school","hetel","park","bank","police","highway","tourism","shop","mark"};
	/**��ͨ*/
	final static int NODE_TYPE_ORDINARY=0;
	/**ҽԺ*/
	final static int NODE_TYPE_HOSPITAL=1;
	/**ѧУ*/
	final static int NODE_TYPE_SCHOOL=2;
	/**�Ƶ�*/
	final static int NODE_TYPE_HETEL=3;
	/**��԰*/
	final static int NODE_TYPE_PARK=4;
	/**����*/
	final static int NODE_TYPE_BANK=5;
	/**�����*/
	final static int NODE_TYPE_POLICE=6;
	/**��·��ͨ�ź�*/
	final static int NODE_TYPE_TRAFFIC=7;
	/**���ξ���*/
	final static int NODE_TYPE_TOURISM=8;
	/**���ﳡ��*/
	final static int NODE_TYPE_SHOP=9;
	final static int NODE_TYPE_MARK = 10;
	
	private QRectF boundingRect = new QRectF(0,0,10,10);
	
	/**�ص�ID,Ĭ��-1*/
	private int nodeID=-1;
	
	/**����,Ĭ��125*/
	private double longitude=125;	
	/**γ��,Ĭ��125*/
	private double latitude=125;
	
	/**�ص����,�������ô���Ĭ��""���ַ�*/
	private String name="";
	
	/**�ص�����ͣ���ҽԺ��ѧϰ�ȣ�Ĭ��ΪNODE_TYPE_ORDINARY*/
	private int type=NODE_TYPE_ORDINARY;
	
	/**��ǩ��Ĭ��Ϊnull*/
	private ArrayList<String> tagArray=null;
	
	/**�õص��ͼ��*/
	private QPixmap pic=new QPixmap();
	
	private QPen pen=new QPen(QColor.black, 6, Qt.PenStyle.SolidLine);

	public MyNode(int id,double lon,double lat,String argument_name, int argu_type){
		nodeID=id;
		longitude=lon;
		latitude=lat;
		this.name = argument_name;
		this.type = argu_type;
		//tagArray=tags;
		
		initShape();
	}
	

	/**��ݵص����������Ӧ����ͼƬ*/
	private void initShape(){
		switch(type){
		case NODE_TYPE_ORDINARY:
			//��ɫԲȦ
			pic.load("images/ordinary.png");
			break;
		case NODE_TYPE_HOSPITAL:
			//��ʮ��
			pic.load("images/hospital.png");
			pen.setColor(QColor.red);
			break;
		case NODE_TYPE_SCHOOL:
			pic.load("images/school.png");
			break;
		case NODE_TYPE_HETEL:
			pic.load("images/hotel.png");
			break;
		
		case NODE_TYPE_PARK:
			pic.load("images/park.png");
			pen.setColor(QColor.darkGreen);
			break;
		case NODE_TYPE_BANK:
			pic.load("images/bank.png");
			pen.setColor(QColor.darkYellow);
			break;
		case NODE_TYPE_POLICE:
			pic.load("images/police.png");
			pen.setColor(QColor.red);
			break;
		case NODE_TYPE_TRAFFIC:
			pic.load("images/trafficlight.png");
			break;
		case NODE_TYPE_TOURISM:
			pic.load("images/tourism.png");
			pen.setColor(QColor.darkBlue);
			break;
		case NODE_TYPE_SHOP:
			pic.load("images/shop.png");
			pen.setColor(QColor.darkMagenta);
			break;
		case NODE_TYPE_MARK:
			pic.load("images/mark.png");
			break;
		default:
			//��ɫԲȦ
			pic.load("images/ordinary.png");
			break;
		}
	}
	
	/**���ڲ���ʱ����������*/
	public void setType(int t){
		type=t;
		initShape();
	}
	
	public int getNodeType(){
		return type;
	}
	
	public int getNodeId(){
		return nodeID;
	}
	
	public String getName(){
		return name;
	}
	
	public double getLongitude(){
		return longitude;
	}
	
	public double getLatitude(){
		return latitude;
	}

	@Override
	public QRectF boundingRect() {
		// TODO Auto-generated method stub
		return boundingRect;
	}

	final static int LEN = 12;
	@Override
	public void paint(QPainter arg0, QStyleOptionGraphicsItem arg1, QWidget arg2) {
		// TODO Auto-generated method stub
		if(type == NODE_TYPE_MARK)
			arg0.drawPixmap(-LEN, -LEN, LEN*2, LEN*2, pic);
		else {
			arg0.drawPixmap(0, 0, LEN, LEN, pic);
		}
		paintName(arg0);
	}
	
	QPointF textPointF = new QPointF();
	private void paintName(QPainter arg0) {
//		if (name.length()<=0)
//			return;
		arg0.setPen(pen);
//		System.out.println(name);
//		System.out.println(type);
		arg0.drawText(0, LEN, name);
	}
}
