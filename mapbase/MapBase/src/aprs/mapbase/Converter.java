package aprs.mapbase;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import java.io.*;
import java.util.*;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Converter{
	//标签类的String和tagItem的互相映射
	final static int MAX_Tag=20;
	// init会读osm文件跑出所有的tag，或者用init1，只人肉了常见的几个tag
	static Map<String, Integer> OsmStrToIntNode, OsmStrToIntWay;
	
	public static void init_RenRou() {
		OsmStrToIntNode = new HashMap<String, Integer>();
		OsmStrToIntNode.clear();
		OsmStrToIntWay = new HashMap<String, Integer>();
		OsmStrToIntWay.clear();
		OsmStrToIntNode.put("highway", 0);
		OsmStrToIntNode.put("amenity", 1);
		OsmStrToIntNode.put("name",    2);
		OsmStrToIntNode.put("address", 3);
		OsmStrToIntNode.put("contact:phone", 4);
		OsmStrToIntNode.put("addr:housenumber", 5);
		OsmStrToIntNode.put("addr:street", 6);

		OsmStrToIntWay.put("highway", 0);
		OsmStrToIntWay.put("cycleway", 1);
		OsmStrToIntWay.put("bridge", 2);
		OsmStrToIntWay.put("railway", 3);
		OsmStrToIntWay.put("waterway", 4);
		OsmStrToIntWay.put("foot", 5);
		OsmStrToIntWay.put("oneway", 6);
		OsmStrToIntWay.put("amenity", 7);
		OsmStrToIntWay.put("bicycle", 8);
		OsmStrToIntWay.put("height", 9);
	}
	
	public static void init() throws ParserConfigurationException, SAXException, IOException{
		OsmStrToIntNode = new HashMap<String, Integer>();
		OsmStrToIntNode.clear();
		OsmStrToIntWay = new HashMap<String, Integer>();
		OsmStrToIntWay.clear();
		
		int lnode = 0, lway = 0;
		
		       DocumentBuilderFactory  factory=DocumentBuilderFactory.newInstance();
		       DocumentBuilder builder=factory.newDocumentBuilder();
		       Document document=builder.parse(new File("mp.osm"));
		       Element root = document.getDocumentElement();
		       NodeList nodes = root.getElementsByTagName("node"),
		       			ways = root.getElementsByTagName("way");
		       for (int j = 0; j < ((5000 < nodes.getLength())?5000:nodes.getLength()); ++j) {
		    	   Node _node = nodes.item(j);
		    	   if (Node.ELEMENT_NODE == _node.getNodeType()) {
		    		   Element zh = (Element)_node;
		    		   //String content2=zh.getAttribute("id");
		    		   //System.out.println(content2);
		    		   NodeList tag = zh.getElementsByTagName("tag");
		    		   for (int k = 0; k < tag.getLength(); ++k) {
		    			   Node _tag = tag.item(k);
		    			   if (Node.ELEMENT_NODE == _tag.getNodeType()) {
		    				   Element zh1 = (Element)_tag;
				    		   String content=zh1.getAttribute("k");
				    		   if (!OsmStrToIntNode.containsKey(content)) {
				    			   //System.out.println(content + ":" + lnode);
				    			   OsmStrToIntNode.put(content, lnode++);
				    		   }
		    			   }
		    		   }
		    	   }
		       }
		       System.out.println(ways.getLength());
		       for (int j = 0; j < ways.getLength(); ++j) {
		    	   Node _node = ways.item(j);
		    	   if (Node.ELEMENT_NODE == _node.getNodeType()) {
		    		   Element zh = (Element)_node;
		    		   //String content2=zh.getAttribute("id");
		    		   //System.out.println(content2);
		    		   NodeList tag = zh.getElementsByTagName("tag");
		    		   for (int k = 0; k < tag.getLength(); ++k) {
		    			   Node _tag = tag.item(k);
		    			   if (Node.ELEMENT_NODE == _tag.getNodeType()) {
		    				   Element zh1 = (Element)_tag;
				    		   String content=zh1.getAttribute("k");
				    		   if (!OsmStrToIntWay.containsKey(content)) {
				    			   //System.out.println(content + ":" + lway);
				    			   OsmStrToIntWay.put(content, lway++);
				    		   }
		    			   }
		    		   }
		    	   }
		       }
	   //System.out.println(lnode + ":" + lway + "\nCreate map finished");
	}	
	
	public static int getItemNode(String item){
		if (OsmStrToIntNode.containsKey(item)) 
			return OsmStrToIntNode.get(item);		
		return -1;
	}
	public static int getItemWay(String item){
		if (OsmStrToIntWay.containsKey(item)) 
			return OsmStrToIntWay.get(item);
		return -1;
	}
}
