package aprs.mapbase;

import java.util.ArrayList;

import com.trolltech.qt.gui.QAction;
import com.trolltech.qt.gui.QApplication;
import com.trolltech.qt.gui.QIcon;
import com.trolltech.qt.gui.QMainWindow;

public class MainWindow extends QMainWindow {
	private MyMap map;
    Ui_MainWindow ui = new Ui_MainWindow();
	private QAction saveAction;
	private QAction exitAction;
	private QAction helpAction;

    public static void main(String[] args) {
        QApplication.initialize(args);

        MainWindow testMainWindow = new MainWindow();
        testMainWindow.show();
        QApplication.exec();
    }

    private void initToolBars(){
    	saveAction = new QAction(tr("&Save"), this);
    	saveAction.setIcon(new QIcon("images/save.png"));
    	saveAction.setStatusTip(tr("Create a new spreadsheet file"));
    	ui.toolBar.addAction(saveAction);
    	
    	exitAction = new QAction(tr("&Exit"), this);
    	exitAction.setIcon(new QIcon("images/close.png"));
    	exitAction.setStatusTip(tr("Exit the application"));
    	ui.toolBar.addAction(exitAction);
    	
    	helpAction=new QAction(tr("&Help"),this);
    	helpAction.setIcon(new QIcon("images/help.png"));
    	helpAction.setStatusTip(tr("User's Manual"));
    	ui.toolBar.addAction(helpAction);
    	initMenu();
    }
    
    private void initMenu(){
    	ui.menuFile.addAction(saveAction);
    	ui.menuFile.addAction(exitAction);
    	ui.menuHelp.addAction(helpAction);
    }
    
    public MainWindow() {
        ui.setupUi(this);
        setupView();
        initToolBars();
        initConnect();
        setWindowIcon(new QIcon("images/map.png"));
        setStatusTip(tr("Welcome!"));
    }

    private void setupView() {
		// TODO Auto-generated method stub
		map = new MyMap(this);
        setCentralWidget(map);
	}
    
    private void initConnect(){
    	
    	ui.locatorClear.clicked.connect(this, "clearLocate()");
    	ui.locatorSearch.clicked.connect(this,"locate()");
    	ui.trackClear.clicked.connect(this,"clearTrack()");
    	ui.trackShowAll.clicked.connect(this,"showTrack()");
    	exitAction.triggered.connect(this,"close()");
    }
    
    /**����ͼ����ʾ�Ĺ켣ȫ�����*/
    public void clearTrack(){
    	map.clearTracks();
    }
    
    /**����ͼ�ϱ�ǵĵص�ȫ�����*/
    public void clearLocate(){
    	map.clearLocator();
    }
    
    /**Ѱ��ĳ�ص㣬����ص�����*/
    public void locate(){
    	map.locate(ui.locatorEdit.text());
    }
    
    /**��ʾ��켣*/
    public void showTrack(){
    	ArrayList<MyNode> track=new ArrayList<MyNode>();
    	/*track.add(new MyNode(-1,ui.longitude1.value(),ui.latitude1.value(),null));
    	track.add(new MyNode(-1,ui.longitude2.value(),ui.latitude2.value(),null));
    	track.add(new MyNode(-1,ui.longitude3.value(),ui.latitude3.value(),null));
    	track.add(new MyNode(-1,ui.longitude4.value(),ui.latitude4.value(),null));
    	track.add(new MyNode(-1,ui.longitude5.value(),ui.latitude5.value(),null));
    	track.add(new MyNode(-1,ui.longitude6.value(),ui.latitude6.value(),null));
    	track.add(new MyNode(-1,ui.longitude7.value(),ui.latitude7.value(),null));*/
    	//map.drawWay(track);
    }
}