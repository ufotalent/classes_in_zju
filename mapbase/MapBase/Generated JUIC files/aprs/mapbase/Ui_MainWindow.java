/********************************************************************************
 ** Form generated from reading ui file 'MainWindow.jui'
 **
 ** Created by: Qt User Interface Compiler version 4.7.1
 **
 ** WARNING! All changes made in this file will be lost when recompiling ui file!
 ********************************************************************************/
package aprs.mapbase;

import com.trolltech.qt.core.*;
import com.trolltech.qt.gui.*;

public class Ui_MainWindow implements com.trolltech.qt.QUiForm<QMainWindow>
{
    public QAction actionSave;
    public QAction actionExit;
    public QWidget centralwidget;
    public QGridLayout gridLayout_3;
    public QMenuBar menubar;
    public QMenu menuFile;
    public QMenu menuHelp;
    public QStatusBar statusbar;
    public QDockWidget dockWidget;
    public QWidget dockWidgetContents;
    public QGridLayout gridLayout_4;
    public QGroupBox groupBox;
    public QGridLayout gridLayout;
    public QLineEdit trackEdit;
    public QPushButton trackClear;
    public QPushButton trackShowAll;
    public QPushButton trackSearch;
    public QGroupBox groupBox_1;
    public QGridLayout gridLayout_5;
    public QLineEdit locatorEdit;
    public QPushButton locatorClear;
    public QPushButton locatorSearch;
    public QGroupBox groupBox_2;
    public QGridLayout gridLayout_6;
    public QComboBox showLastHours;
    public QGroupBox groupBox_3;
    public QGridLayout gridLayout_2;
    public QLabel label;
    public QLabel label_2;
    public QDoubleSpinBox longitude1;
    public QDoubleSpinBox latitude1;
    public QDoubleSpinBox longitude2;
    public QDoubleSpinBox latitude2;
    public QDoubleSpinBox longitude3;
    public QDoubleSpinBox latitude3;
    public QDoubleSpinBox longitude4;
    public QDoubleSpinBox latitude4;
    public QDoubleSpinBox longitude5;
    public QDoubleSpinBox latitude5;
    public QDoubleSpinBox longitude6;
    public QDoubleSpinBox latitude6;
    public QDoubleSpinBox longitude7;
    public QDoubleSpinBox latitude7;
    public QPushButton btnShowTrack;
    public QToolBar toolBar;

    public Ui_MainWindow() { super(); }

    public void setupUi(QMainWindow MainWindow)
    {
        MainWindow.setObjectName("MainWindow");
        MainWindow.resize(new QSize(924, 657).expandedTo(MainWindow.minimumSizeHint()));
        MainWindow.setWindowIcon(new QIcon(new QPixmap("../../../images/map.png")));
        actionSave = new QAction(MainWindow);
        actionSave.setObjectName("actionSave");
        actionSave.setIcon(new QIcon(new QPixmap("../../images/save.png")));
        actionExit = new QAction(MainWindow);
        actionExit.setObjectName("actionExit");
        actionExit.setIcon(new QIcon(new QPixmap("../../images/close.png")));
        centralwidget = new QWidget(MainWindow);
        centralwidget.setObjectName("centralwidget");
        gridLayout_3 = new QGridLayout(centralwidget);
        gridLayout_3.setObjectName("gridLayout_3");
        MainWindow.setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar.setObjectName("menubar");
        menubar.setGeometry(new QRect(0, 0, 924, 23));
        menuFile = new QMenu(menubar);
        menuFile.setObjectName("menuFile");
        menuHelp = new QMenu(menubar);
        menuHelp.setObjectName("menuHelp");
        MainWindow.setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar.setObjectName("statusbar");
        MainWindow.setStatusBar(statusbar);
        dockWidget = new QDockWidget(MainWindow);
        dockWidget.setObjectName("dockWidget");
        dockWidgetContents = new QWidget();
        dockWidgetContents.setObjectName("dockWidgetContents");
        gridLayout_4 = new QGridLayout(dockWidgetContents);
        gridLayout_4.setObjectName("gridLayout_4");
        groupBox = new QGroupBox(dockWidgetContents);
        groupBox.setObjectName("groupBox");
        gridLayout = new QGridLayout(groupBox);
        gridLayout.setObjectName("gridLayout");
        trackEdit = new QLineEdit(groupBox);
        trackEdit.setObjectName("trackEdit");

        gridLayout.addWidget(trackEdit, 0, 0, 1, 2);

        trackClear = new QPushButton(groupBox);
        trackClear.setObjectName("trackClear");

        gridLayout.addWidget(trackClear, 1, 0, 1, 1);

        trackShowAll = new QPushButton(groupBox);
        trackShowAll.setObjectName("trackShowAll");

        gridLayout.addWidget(trackShowAll, 1, 1, 1, 1);

        trackSearch = new QPushButton(groupBox);
        trackSearch.setObjectName("trackSearch");

        gridLayout.addWidget(trackSearch, 1, 2, 1, 1);


        gridLayout_4.addWidget(groupBox, 0, 0, 1, 1);

        groupBox_1 = new QGroupBox(dockWidgetContents);
        groupBox_1.setObjectName("groupBox_1");
        gridLayout_5 = new QGridLayout(groupBox_1);
        gridLayout_5.setObjectName("gridLayout_5");
        locatorEdit = new QLineEdit(groupBox_1);
        locatorEdit.setObjectName("locatorEdit");

        gridLayout_5.addWidget(locatorEdit, 0, 0, 1, 2);

        locatorClear = new QPushButton(groupBox_1);
        locatorClear.setObjectName("locatorClear");

        gridLayout_5.addWidget(locatorClear, 1, 0, 1, 1);

        locatorSearch = new QPushButton(groupBox_1);
        locatorSearch.setObjectName("locatorSearch");

        gridLayout_5.addWidget(locatorSearch, 1, 1, 1, 1);


        gridLayout_4.addWidget(groupBox_1, 1, 0, 1, 1);

        groupBox_2 = new QGroupBox(dockWidgetContents);
        groupBox_2.setObjectName("groupBox_2");
        gridLayout_6 = new QGridLayout(groupBox_2);
        gridLayout_6.setObjectName("gridLayout_6");
        showLastHours = new QComboBox(groupBox_2);
        showLastHours.setObjectName("showLastHours");

        gridLayout_6.addWidget(showLastHours, 0, 0, 1, 1);


        gridLayout_4.addWidget(groupBox_2, 2, 0, 1, 1);

        groupBox_3 = new QGroupBox(dockWidgetContents);
        groupBox_3.setObjectName("groupBox_3");
        gridLayout_2 = new QGridLayout(groupBox_3);
        gridLayout_2.setObjectName("gridLayout_2");
        label = new QLabel(groupBox_3);
        label.setObjectName("label");

        gridLayout_2.addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(groupBox_3);
        label_2.setObjectName("label_2");

        gridLayout_2.addWidget(label_2, 0, 1, 1, 1);

        longitude1 = new QDoubleSpinBox(groupBox_3);
        longitude1.setObjectName("longitude1");

        gridLayout_2.addWidget(longitude1, 1, 0, 1, 1);

        latitude1 = new QDoubleSpinBox(groupBox_3);
        latitude1.setObjectName("latitude1");

        gridLayout_2.addWidget(latitude1, 1, 1, 1, 1);

        longitude2 = new QDoubleSpinBox(groupBox_3);
        longitude2.setObjectName("longitude2");

        gridLayout_2.addWidget(longitude2, 2, 0, 1, 1);

        latitude2 = new QDoubleSpinBox(groupBox_3);
        latitude2.setObjectName("latitude2");

        gridLayout_2.addWidget(latitude2, 2, 1, 1, 1);

        longitude3 = new QDoubleSpinBox(groupBox_3);
        longitude3.setObjectName("longitude3");

        gridLayout_2.addWidget(longitude3, 3, 0, 1, 1);

        latitude3 = new QDoubleSpinBox(groupBox_3);
        latitude3.setObjectName("latitude3");

        gridLayout_2.addWidget(latitude3, 3, 1, 1, 1);

        longitude4 = new QDoubleSpinBox(groupBox_3);
        longitude4.setObjectName("longitude4");

        gridLayout_2.addWidget(longitude4, 4, 0, 1, 1);

        latitude4 = new QDoubleSpinBox(groupBox_3);
        latitude4.setObjectName("latitude4");

        gridLayout_2.addWidget(latitude4, 4, 1, 1, 1);

        longitude5 = new QDoubleSpinBox(groupBox_3);
        longitude5.setObjectName("longitude5");

        gridLayout_2.addWidget(longitude5, 5, 0, 1, 1);

        latitude5 = new QDoubleSpinBox(groupBox_3);
        latitude5.setObjectName("latitude5");

        gridLayout_2.addWidget(latitude5, 5, 1, 1, 1);

        longitude6 = new QDoubleSpinBox(groupBox_3);
        longitude6.setObjectName("longitude6");

        gridLayout_2.addWidget(longitude6, 6, 0, 1, 1);

        latitude6 = new QDoubleSpinBox(groupBox_3);
        latitude6.setObjectName("latitude6");

        gridLayout_2.addWidget(latitude6, 6, 1, 1, 1);

        longitude7 = new QDoubleSpinBox(groupBox_3);
        longitude7.setObjectName("longitude7");

        gridLayout_2.addWidget(longitude7, 7, 0, 1, 1);

        latitude7 = new QDoubleSpinBox(groupBox_3);
        latitude7.setObjectName("latitude7");

        gridLayout_2.addWidget(latitude7, 7, 1, 1, 1);

        btnShowTrack = new QPushButton(groupBox_3);
        btnShowTrack.setObjectName("btnShowTrack");

        gridLayout_2.addWidget(btnShowTrack, 8, 0, 1, 2);


        gridLayout_4.addWidget(groupBox_3, 3, 0, 1, 1);

        dockWidget.setWidget(dockWidgetContents);
        MainWindow.addDockWidget(com.trolltech.qt.core.Qt.DockWidgetArea.resolve(2), dockWidget);
        toolBar = new QToolBar(MainWindow);
        toolBar.setObjectName("toolBar");
        MainWindow.addToolBar(com.trolltech.qt.core.Qt.ToolBarArea.TopToolBarArea, toolBar);

        menubar.addAction(menuFile.menuAction());
        menubar.addAction(menuHelp.menuAction());
        retranslateUi(MainWindow);

        MainWindow.connectSlotsByName();
    } // setupUi

    void retranslateUi(QMainWindow MainWindow)
    {
        MainWindow.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "APRS", null));
        actionSave.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Save", null));
        actionExit.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Exit", null));
        menuFile.setTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "File", null));
        menuHelp.setTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Help", null));
        groupBox.setTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Track callsign", null));
        trackClear.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Clear", null));
        trackShowAll.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Show All", null));
        trackSearch.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Search", null));
        groupBox_1.setTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Address/city/locator", null));
        locatorClear.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Clear", null));
        locatorSearch.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Search", null));
        groupBox_2.setTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Show Last Hours", null));
        groupBox_3.setTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "\u663e\u793a\u8f68\u8ff9\uff08MapViewer\u9700\u8981\u4f20\u8fdb\u8f68\u8ff9\uff09", null));
        label.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "longitude", null));
        label_2.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "latitude", null));
        btnShowTrack.setText(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "Show the track", null));
        toolBar.setWindowTitle(com.trolltech.qt.core.QCoreApplication.translate("MainWindow", "toolBar", null));
    } // retranslateUi

}

